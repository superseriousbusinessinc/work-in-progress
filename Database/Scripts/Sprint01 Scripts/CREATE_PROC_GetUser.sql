DELIMITER //

DROP PROCEDURE IF EXISTS `getUser` //

CREATE PROCEDURE `getUser` (`username` VARCHAR(50), `password` VARCHAR(50))
BEGIN
	SELECT * FROM ssbi.user
    WHERE ssbi.user.Username = `username` AND ssbi.user.Password = `password`;
END//

DROP PROCEDURE IF EXISTS `getUserByUsername` //

CREATE PROCEDURE `getUserByUsername` (`username` VARCHAR(50))
BEGIN
	SELECT * FROM ssbi.user
    WHERE ssbi.user.Username = `username`;
END//

DELIMITER ;