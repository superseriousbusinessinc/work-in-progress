USE SSBI;

/* DELETE USER */
DELETE FROM user WHERE User_ID = 1;
DELETE FROM company WHERE Company_ID = 1;
DELETE FROM paymentinformation WHERE PaymentInformation_ID = 1;
DELETE FROM cardtype WHERE CardType_ID = 1;
DELETE FROM department WHERE Department_ID = 1;
DELETE FROM title WHERE Title_ID = 1;

/* INSERT USER */
INSERT INTO title VALUES (1, 'Mr.');

INSERT INTO department VALUES (1, 'IT');

INSERT INTO cardtype VALUES (1, 'Visa');

INSERT INTO paymentinformation (
	PaymentInformation_ID,
	NameOnCard,
    CardType_FK,
    CardNumber,
    ExpiryDate,
    SecurityNumber
)
VALUES (
	1,
	'Admin Istrator',
    1,
    123456789012345,
    '2016-12-31',
    123
);

INSERT INTO company (
	Company_ID,
	CompanyName,
	Address,
	City,
	Region,
	PostalCode,
	Country,
	MaxUsers,
	CurrentUsers,
	PaymentInformation_FK
)
VALUES (
	1,
	'Super Serious Business Inc.',
    '123 Awesome St.',
    'London',
    'Ontario',
    'N6H 1M9',
    'Canada',
    10,
    0,
    1
);

INSERT INTO user (
	FirstName,
	LastName,
	Password,
	UserName,
    Title_FK,
    Department_FK,
    Company_FK,
	CreatedOn
)
VALUES(
	'Admin',
	'Istrator',
	'admin',
	'administrator',
    1,
    1,
    1,
	NOW()
);

SELECT * FROM user;