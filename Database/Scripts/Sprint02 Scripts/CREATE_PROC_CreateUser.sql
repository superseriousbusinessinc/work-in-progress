DELIMITER //

USE `ssbi`//

DROP PROCEDURE IF EXISTS `createUser` //
CREATE PROCEDURE `createUser` (
	in `firstname` VARCHAR(50),
	in `lastname` VARCHAR(50),
	in `password` VARCHAR(10),
	in `username` VARCHAR(25),
	in `title_fk` INT,
	in `department_fk` INT,
	in `company_fk` INT
   )
BEGIN

	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
    BEGIN 
          ROLLBACK;
    END;
    
	Start Transaction;

	INSERT INTO user (
		FirstName, 
		LastName, 
		Password, 
		UserName,
		Title_FK,
		Department_FK,
		Company_FK,
		CreatedOn)
		VALUES (`firstname`,
		`lastname`,
		`password`,
		`username`,
		`title_fk`,
		`department_fk`,
		`company_fk`,
		NOW()
	);

	Commit;

	SELECT LAST_INSERT_ID() AS `User_ID`;
  
END//

DELIMITER ;