DELIMITER $$
USE `ssbi`$$
DROP procedure IF EXISTS `getCurrentUserProjects`$$
CREATE PROCEDURE `getCurrentUserProjects`(In UserID int)
BEGIN
	SELECT project.*
	FROM project
		JOIN userproject
			ON project.Project_ID = userproject.Project_FK
		JOIN user
		ON user.User_ID = userproject.User_FK
	Where user.User_ID = UserID
	Order by project.DueDate ASC ;
END$$

DROP procedure IF EXISTS `createProject`$$
CREATE PROCEDURE `createProject` (in projectName varchar(45),in dateDue datetime,in type_FK int, in priority_FK int,in status_FK int, in company_FK int, in userID int)
BEGIN

	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
    BEGIN 
          ROLLBACK;
    END;
    
	Start Transaction;
    
	insert into project(ProjectName,DateStarted,DueDate,Type_FK,Priority_FK,Status_FK,Company_FK)
		values(projectName,now(),dateDue,type_FK,priority_FK,status_FK,company_FK); 

	SET @newProjectID = LAST_INSERT_ID();

	insert into userproject(User_FK,Project_FK)
		values(userid,@newProjectID);

	Commit;
    
    SELECT @newProjectID;

END$$

DELIMITER ;