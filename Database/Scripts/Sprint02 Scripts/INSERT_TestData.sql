USE SSBI;

/* DELETE ALL*/
DELETE FROM userproject WHERE User_FK > 0;
DELETE FROM project WHERE Company_FK > 0;
DELETE FROM projectstatus WHERE ProjectStatus_ID > 0;
DELETE FROM priorityofproject WHERE PriorityOfProject_ID > 0;
DELETE FROM typeofproject WHERE TypeOfProject_ID > 0;

DELETE FROM user WHERE User_ID > 0;
DELETE FROM company WHERE Company_ID > 0;
DELETE FROM paymentinformation WHERE PaymentInformation_ID > 0;
DELETE FROM cardtype WHERE CardType_ID > 0;
DELETE FROM department WHERE Department_ID > 0;
DELETE FROM title WHERE Title_ID > 0;


/* INSERT USER */
INSERT INTO title (TitleName) VALUES ( 'Mr.');
SET @titleID = LAST_INSERT_ID();
INSERT INTO title (TitleName) VALUES ( 'Mrs.');
INSERT INTO title (TitleName) VALUES ( 'Ms.');
INSERT INTO title (TitleName) VALUES ( 'Dr.');

INSERT INTO department (DepartmentName) VALUES ( 'IT');
SET @departmentID = LAST_INSERT_ID();

INSERT INTO cardtype (CardType) VALUES ( 'Visa');
SET @cardTypeID = LAST_INSERT_ID();
INSERT INTO cardtype (CardType) VALUES ( 'MasterCard');

INSERT INTO paymentinformation (
	NameOnCard,
    CardType_FK,
    CardNumber,
    ExpiryDate,
    SecurityNumber
)
VALUES (
	'Admin Istrator',
    @cardTypeID,
    123456789012345,
    '2016-12-31',
    123
);
SET @paymentInfoID = LAST_INSERT_ID();

INSERT INTO company (
	CompanyName,
	Address,
	City,
	Region,
	PostalCode,
	Country,
	MaxUsers,
	CurrentUsers,
	PaymentInformation_FK
)
VALUES (
	'Super Serious Business Inc.',
    '123 Awesome St.',
    'London',
    'Ontario',
    'N6H 1M9',
    'Canada',
    10,
    0,
    @paymentInfoID
);
SET @companyID = LAST_INSERT_ID();

INSERT INTO user (
	FirstName,
	LastName,
	Password,
	UserName,
    Title_FK,
    Department_FK,
    Company_FK,
	CreatedOn
)
VALUES(
	'Admin',
	'Istrator',
	'admin',
	'administrator',
    @titleID,
    @departmentID,
    @companyID,
	NOW()
);
SET @userID = LAST_INSERT_ID();


-- ProjectStatus table inserts
insert into ssbi.projectstatus(ProjectStatus) values ('Open');
SET @ps1 = LAST_INSERT_ID();
insert into ssbi.projectstatus(ProjectStatus) values ('Closed');
SET @ps2 = LAST_INSERT_ID();
insert into ssbi.projectstatus(ProjectStatus) values ('Active');
SET @ps3 = LAST_INSERT_ID();
insert into ssbi.projectstatus(ProjectStatus) values ('Cancelled');
SET @ps4 = LAST_INSERT_ID();
-- PriorityOfProject table inserts
insert into ssbi.priorityofproject(PriorityOfProject) values ('Low');
SET @pp1 = LAST_INSERT_ID();
insert into ssbi.priorityofproject(PriorityOfProject) values ('Medium');
SET @pp2 = LAST_INSERT_ID();
insert into ssbi.priorityofproject(PriorityOfProject) values ('High');
SET @pp3 = LAST_INSERT_ID();
insert into ssbi.priorityofproject(PriorityOfProject) values ('Urgent');
SET @pp4 = LAST_INSERT_ID();
-- TypeOfProject table inserts
insert into ssbi.typeofproject(TypeOfProject) values ('Planning');
SET @projType1 = LAST_INSERT_ID();
insert into ssbi.typeofproject(TypeOfProject) values ('InHouse');
SET @projType2 = LAST_INSERT_ID();
insert into ssbi.typeofproject(TypeOfProject) values ('Customer');
SET @projType3 = LAST_INSERT_ID();


insert into ssbi.project(ProjectName,DateStarted,DueDate,Type_FK,Priority_FK,Status_FK,Company_FK)
values('AwesomeSauce',Now(),Now() + 10,@projType1,@pp1,@pp1,@companyID);
SET @proj1 = LAST_INSERT_ID();

insert into ssbi.project(ProjectName,DateStarted,DueDate,Type_FK,Priority_FK,Status_FK,Company_FK)
values('AwesomeSauce2.0',Now(),Now() + 20,@projType2,@pp2,@pp1,@companyID);
SET @proj2 = LAST_INSERT_ID();

insert into ssbi.userproject(User_FK,Project_FK)
values(@userID, @proj1);
