-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema SSBI
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `SSBI` ;

-- -----------------------------------------------------
-- Schema SSBI
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `SSBI` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `SSBI` ;

-- -----------------------------------------------------
-- Table `SSBI`.`CardType`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SSBI`.`CardType` (
  `CardType_ID` INT NOT NULL AUTO_INCREMENT,
  `CardType` VARCHAR(25) NOT NULL,
  PRIMARY KEY (`CardType_ID`),
  UNIQUE INDEX `CardType_ID_UNIQUE` (`CardType_ID` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `SSBI`.`PaymentInformation`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SSBI`.`PaymentInformation` (
  `PaymentInformation_ID` INT NOT NULL AUTO_INCREMENT,
  `NameOnCard` VARCHAR(50) NOT NULL,
  `CardType_FK` INT NOT NULL,
  `CardNumber` VARCHAR(15) NOT NULL,
  `ExpiryDate` DATETIME NOT NULL,
  `SecurityNumber` VARCHAR(4) NOT NULL,
  PRIMARY KEY (`PaymentInformation_ID`),
  INDEX `CardType_FK_idx` (`CardType_FK` ASC),
  CONSTRAINT `CardType_FK`
    FOREIGN KEY (`CardType_FK`)
    REFERENCES `SSBI`.`CardType` (`CardType_ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `SSBI`.`Company`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SSBI`.`Company` (
  `Company_ID` INT NOT NULL AUTO_INCREMENT,
  `CompanyName` VARCHAR(45) NOT NULL,
  `Address` VARCHAR(45) NOT NULL,
  `City` VARCHAR(45) NOT NULL,
  `Region` VARCHAR(45) NOT NULL,
  `PostalCode` VARCHAR(45) NOT NULL,
  `Country` VARCHAR(45) NOT NULL,
  `MaxUsers` INT NOT NULL,
  `CurrentUsers` INT NOT NULL,
  `PaymentInformation_FK` INT NOT NULL,
  PRIMARY KEY (`Company_ID`),
  INDEX `PaymentInformation_FK_idx` (`PaymentInformation_FK` ASC),
  CONSTRAINT `PaymentInformation_FK`
    FOREIGN KEY (`PaymentInformation_FK`)
    REFERENCES `SSBI`.`PaymentInformation` (`PaymentInformation_ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `SSBI`.`Department`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SSBI`.`Department` (
  `Department_ID` INT NOT NULL AUTO_INCREMENT,
  `DepartmentName` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`Department_ID`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `SSBI`.`Title`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SSBI`.`Title` (
  `Title_ID` INT NOT NULL AUTO_INCREMENT,
  `TitleName` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`Title_ID`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `SSBI`.`User`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SSBI`.`User` (
  `User_ID` INT NOT NULL AUTO_INCREMENT,
  `FirstName` VARCHAR(50) NOT NULL,
  `LastName` VARCHAR(50) NOT NULL,
  `Password` VARCHAR(10) NOT NULL,
  `UserName` VARCHAR(25) NOT NULL,
  `Title_FK` INT NOT NULL,
  `Department_FK` INT NOT NULL,
  `Company_FK` INT NOT NULL,
  `CreatedOn` DATETIME NOT NULL,
  PRIMARY KEY (`User_ID`),
  INDEX `Title_FK_idx` (`Title_FK` ASC),
  INDEX `Deparment_FK_idx` (`Department_FK` ASC),
  INDEX `Company_FK_idx` (`Company_FK` ASC),
  CONSTRAINT `Title_FK`
    FOREIGN KEY (`Title_FK`)
    REFERENCES `SSBI`.`Title` (`Title_ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `Deparment_FK`
    FOREIGN KEY (`Department_FK`)
    REFERENCES `SSBI`.`Department` (`Department_ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `CompanyUser_FK`
    FOREIGN KEY (`Company_FK`)
    REFERENCES `SSBI`.`Company` (`Company_ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `SSBI`.`IssueStatus`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SSBI`.`IssueStatus` (
  `IssueStatus_ID` INT NOT NULL AUTO_INCREMENT,
  `IssueStatus` VARCHAR(10) NOT NULL,
  PRIMARY KEY (`IssueStatus_ID`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `SSBI`.`TypeOfIssue`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SSBI`.`TypeOfIssue` (
  `TypeOfIssue_ID` INT NOT NULL AUTO_INCREMENT,
  `TypeOfIssue` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`TypeOfIssue_ID`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `SSBI`.`PriorityOfIssue`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SSBI`.`PriorityOfIssue` (
  `PriorityOfIssue_ID` INT NOT NULL AUTO_INCREMENT,
  `PriorityOfIssue` VARCHAR(10) NOT NULL,
  PRIMARY KEY (`PriorityOfIssue_ID`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `SSBI`.`TypeOfProject`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SSBI`.`TypeOfProject` (
  `TypeOfProject_ID` INT NOT NULL AUTO_INCREMENT,
  `TypeOfProject` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`TypeOfProject_ID`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `SSBI`.`PriorityOfProject`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SSBI`.`PriorityOfProject` (
  `PriorityOfProject_ID` INT NOT NULL AUTO_INCREMENT,
  `PriorityOfProject` VARCHAR(10) NOT NULL,
  PRIMARY KEY (`PriorityOfProject_ID`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `SSBI`.`ProjectStatus`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SSBI`.`ProjectStatus` (
  `ProjectStatus_ID` INT NOT NULL AUTO_INCREMENT,
  `ProjectStatus` VARCHAR(20) NOT NULL,
  PRIMARY KEY (`ProjectStatus_ID`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `SSBI`.`Project`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SSBI`.`Project` (
  `Project_ID` INT NOT NULL AUTO_INCREMENT,
  `ProjectName` VARCHAR(45) NOT NULL,
  `DateStarted` DATETIME NOT NULL,
  `DueDate` DATETIME NOT NULL,
  `Type_FK` INT NOT NULL,
  `Priority_FK` INT NOT NULL,
  `Status_FK` INT NOT NULL,
  `Company_FK` INT NOT NULL,
  PRIMARY KEY (`Project_ID`),
  INDEX `Type_FK_idx` (`Type_FK` ASC),
  INDEX `Priority_FK_idx` (`Priority_FK` ASC),
  INDEX `Status_FK_idx` (`Status_FK` ASC),
  INDEX `Company_FK_idx` (`Company_FK` ASC),
  CONSTRAINT `Type_FK`
    FOREIGN KEY (`Type_FK`)
    REFERENCES `SSBI`.`TypeOfProject` (`TypeOfProject_ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `Priority_FK`
    FOREIGN KEY (`Priority_FK`)
    REFERENCES `SSBI`.`PriorityOfProject` (`PriorityOfProject_ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `Status_FK`
    FOREIGN KEY (`Status_FK`)
    REFERENCES `SSBI`.`ProjectStatus` (`ProjectStatus_ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `CompanyProject_FK`
    FOREIGN KEY (`Company_FK`)
    REFERENCES `SSBI`.`Company` (`Company_ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `SSBI`.`Issue`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SSBI`.`Issue` (
  `Issue_ID` INT NOT NULL AUTO_INCREMENT,
  `IssueName` VARCHAR(30) NOT NULL,
  `DateCreated` DATETIME NOT NULL,
  `EstimatedTime` DATETIME NOT NULL,
  `Difficulty` INT NOT NULL,
  `Status_FK` INT NOT NULL,
  `Type_FK` INT NOT NULL,
  `Priority_FK` INT NOT NULL,
  `Project_FK` INT NOT NULL,
  PRIMARY KEY (`Issue_ID`),
  INDEX `Status_FK_idx` (`Status_FK` ASC),
  INDEX `Type_FK_idx` (`Type_FK` ASC),
  INDEX `Priority_FK_idx` (`Priority_FK` ASC),
  INDEX `Project_FK_idx` (`Project_FK` ASC),
  CONSTRAINT `StatusIssue_FK`
    FOREIGN KEY (`Status_FK`)
    REFERENCES `SSBI`.`IssueStatus` (`IssueStatus_ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `TypeIssue_FK`
    FOREIGN KEY (`Type_FK`)
    REFERENCES `SSBI`.`TypeOfIssue` (`TypeOfIssue_ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `PriorityIssue_FK`
    FOREIGN KEY (`Priority_FK`)
    REFERENCES `SSBI`.`PriorityOfIssue` (`PriorityOfIssue_ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `ProjectIssue_FK`
    FOREIGN KEY (`Project_FK`)
    REFERENCES `SSBI`.`Project` (`Project_ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `SSBI`.`IssueComment`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SSBI`.`IssueComment` (
  `IssueComment_ID` INT NOT NULL AUTO_INCREMENT,
  `Comment` VARCHAR(225) NOT NULL,
  `DatePosted` DATETIME NOT NULL,
  `User_FK` INT NOT NULL,
  `Issue_FK` INT NOT NULL,
  PRIMARY KEY (`IssueComment_ID`),
  INDEX `Issue_FK_idx` (`Issue_FK` ASC),
  INDEX `User_FK_idx` (`User_FK` ASC),
  CONSTRAINT `UserIssue_FK`
    FOREIGN KEY (`User_FK`)
    REFERENCES `SSBI`.`User` (`User_ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `IssueComment_FK`
    FOREIGN KEY (`Issue_FK`)
    REFERENCES `SSBI`.`Issue` (`Issue_ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `SSBI`.`IssueUser`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SSBI`.`IssueUser` (
  `IssueUser_ID` INT NOT NULL AUTO_INCREMENT,
  `Issue_FK` INT NOT NULL,
  `User_FK` INT NOT NULL,
  PRIMARY KEY (`IssueUser_ID`),
  INDEX `Issue_FK_idx` (`Issue_FK` ASC),
  INDEX `User_FK_idx` (`User_FK` ASC),
  CONSTRAINT `Issue_FK`
    FOREIGN KEY (`Issue_FK`)
    REFERENCES `SSBI`.`Issue` (`Issue_ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `User_FK`
    FOREIGN KEY (`User_FK`)
    REFERENCES `SSBI`.`User` (`User_ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `SSBI`.`ProjectComment`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SSBI`.`ProjectComment` (
  `ProjectComment_ID` INT NOT NULL AUTO_INCREMENT,
  `Comment` VARCHAR(225) NOT NULL,
  `DatePosted` DATETIME NOT NULL,
  `User_FK` INT NOT NULL,
  `Project_FK` INT NOT NULL,
  PRIMARY KEY (`ProjectComment_ID`),
  INDEX `User_FK_idx` (`User_FK` ASC),
  INDEX `Project_FK_idx` (`Project_FK` ASC),
  CONSTRAINT `UserProjectComment_FK`
    FOREIGN KEY (`User_FK`)
    REFERENCES `SSBI`.`User` (`User_ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `ProjectComment_FK`
    FOREIGN KEY (`Project_FK`)
    REFERENCES `SSBI`.`Project` (`Project_ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `SSBI`.`UserProject`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `SSBI`.`UserProject` (
  `UserProject_ID` INT NOT NULL AUTO_INCREMENT,
  `User_FK` INT NOT NULL,
  `Project_FK` INT NOT NULL,
  PRIMARY KEY (`UserProject_ID`),
  INDEX `User_FK_idx` (`User_FK` ASC),
  INDEX `Project_FK_idx` (`Project_FK` ASC),
  CONSTRAINT `UserProject_FK`
    FOREIGN KEY (`User_FK`)
    REFERENCES `SSBI`.`User` (`User_ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `ProjectName_FK`
    FOREIGN KEY (`Project_FK`)
    REFERENCES `SSBI`.`Project` (`Project_ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
