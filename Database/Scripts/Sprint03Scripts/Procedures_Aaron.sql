USE `ssbi`;
DROP procedure IF EXISTS `addUserToProject`;

DELIMITER $$
USE `ssbi`$$
CREATE PROCEDURE `addUserToProject` (in projectid int,in userid int )
BEGIN
insert into ssbi.userproject( User_FK, Project_FK )
values( userid, projectid );
END
$$

DELIMITER ;




USE `ssbi`;
DROP procedure IF EXISTS `removeUserFromProject`;

DELIMITER $$
USE `ssbi`$$

Create PROCEDURE `removeUserFromProject`(in projectid int,in userid int )
BEGIN
delete from ssbi.userproject
where userproject.User_FK = userid and userproject.Project_FK = projectid
limit 1;
END

$$

DELIMITER ;

USE `ssbi`;
DROP procedure IF EXISTS `addIssue`;

DELIMITER $$
USE `ssbi`$$
CREATE PROCEDURE `addIssue` (in iName varchar(30), iDate datetime, iEstimatedTime datetime, iDifficulty int, iStatusFK int, iTypeFK int, iPriorityFK int, iProjectFK int)
BEGIN
Insert into ssbi.issue(IssueName,DateCreated,EstimatedTime,Difficulty,Status_FK,Type_FK,Priority_FK,Project_FK)
values(iName,iDate,iEstimatedTime,iDifficulty,iStatusFK,iTypeFK,iPriorityFK,iProjectFK);
END
$$

DELIMITER ;


USE `ssbi`;
DROP procedure IF EXISTS `removeIssue`;

DELIMITER $$
USE `ssbi`$$
CREATE PROCEDURE `removeIssue`(in IssueID int)
BEGIN
Delete from ssbi.issue
where issue.Issue_ID = IssueID
limit 1;
END$$

DELIMITER ;
;
USE `ssbi`;
DROP procedure IF EXISTS `allUsersofCompany`;

DELIMITER $$
USE `ssbi`$$
CREATE PROCEDURE `allUsersofCompany` (in companyFK int)
BEGIN
select * from ssbi.user
where company_FK = companyFK;
END
$$

DELIMITER ;

USE `ssbi`;
DROP procedure IF EXISTS `updateProject`;

DELIMITER $$
USE `ssbi`$$
CREATE PROCEDURE `updateProject` (in projectid int,in projectname varchar(45), in duedate datetime, in typefk int, in priorityfk int, in statusfk int, in companyfk int)
BEGIN
update ssbi.project
set ProjectName = projectname,
	DueDate = duedate,
    Type_FK = typefk,
    Priority_FK = priorityfk,
    Status_FK = statusfk,
    Company_FK = companyfk
where project.Project_ID = projectid;
END
$$

DELIMITER ;

USE `ssbi`;
DROP procedure IF EXISTS `companiesProjects`;

DELIMITER $$
USE `ssbi`$$
CREATE PROCEDURE `companiesProjects` (in currentUserCompanyID int)
BEGIN
SELECT * FROM ssbi.project
join ssbi.userproject 
on ssbi.project.Project_ID = userproject.Project_FK
join ssbi.user
on user.User_ID = userproject.User_FK
join ssbi.company
on ssbi.company.Company_ID = user.Company_FK
where Company_ID = currentUserCompany;
END
$$

DELIMITER ;

USE `ssbi`;
DROP procedure IF EXISTS `addUserToIssue`;

DELIMITER $$
USE `ssbi`$$

CREATE PROCEDURE `addUserToIssue` (in issueid int,in userid int )
BEGIN
    
	insert into ssbi.issueuser( Issue_FK,User_FK  )
	values( issueid, userid );
    
END
$$

DELIMITER ;



USE `ssbi`;
DROP procedure IF EXISTS `removeUserFromIssue`;

DELIMITER $$
USE `ssbi`$$
Create PROCEDURE `removeUserFromIssue`(in issueid int,in userid int )
BEGIN
delete from ssbi.issueuser
where issueuser.User_FK = userid and issueuser.Issue_FK = issueid
limit 1;
END$$

DELIMITER ;
;


USE `ssbi`;
DROP procedure IF EXISTS `ssbi`.`getAllIssuesOfProject`;

DELIMITER $$
USE `ssbi`$$
CREATE PROCEDURE `getAllIssuesOfProject`(in projectfk int)
BEGIN
SELECT * FROM ssbi.Issue
where Project_FK = projectfk;
END$$

DELIMITER ;
;

USE `ssbi`;
DROP procedure IF EXISTS `ssbi`.`getAllIssuesForCurrentUser`;

DELIMITER $$
USE `ssbi`$$
CREATE PROCEDURE `getAllIssuesForCurrentUser`(in userid int)
BEGIN
SELECT * FROM ssbi.issue
join issueuser
on ssbi.issue.Issue_ID = ssbi.issueuser.Issue_FK
where user_FK = userid;

END$$

DELIMITER ;
;


USE `ssbi`;
DROP procedure IF EXISTS `ssbi`.`updateIssue`;

DELIMITER $$
USE `ssbi`$$
CREATE  PROCEDURE `updateIssue`(in issueid int,in issuename varchar(30), in estimatedtime datetime,in difficulty int, in statusfk int, in typefk int , in proiorityfk int, in projectfk int)
BEGIN
update ssbi.issue
set IsssueName = issuename,
	EstimatedTime = estimatedtime,
    Difficulty = difficulty,
    Type_FK = typefk,
    Priority_FK = priorityfk,
    Status_FK = statusfk,
    Project_FK = projectfk
where Issue_ID = issueid;
END$$

DELIMITER ;
;
