/*
CALL getCurrentUserProjects(13);
*/

/*
Call addUserToProject(16, 13);
*/

/*
`addIssue` (in iName varchar(30), iDate datetime, iEstimatedTime datetime, iDifficulty int, iStatusFK int, iTypeFK int, iPriorityFK int, iProjectFK int)
*/
/*
DELETE FROM issuestatus WHERE IssueStatus_ID > 0;
DELETE FROM priorityofissue WHERE PriorityOfIssue_ID > 0;
DELETE FROM typeofissue WHERE TypeOfIssue_ID > 0;

-- ProjectStatus table inserts
insert into ssbi.issuestatus(IssueStatus) values ('Open');
SET @is1 = LAST_INSERT_ID();
insert into ssbi.issuestatus(IssueStatus) values ('Closed');
SET @is2 = LAST_INSERT_ID();
insert into ssbi.issuestatus(IssueStatus) values ('Active');
SET @is3 = LAST_INSERT_ID();
insert into ssbi.issuestatus(IssueStatus) values ('Cancelled');
SET @is4 = LAST_INSERT_ID();
-- PriorityOfProject table inserts
insert into ssbi.priorityofissue(PriorityOfIssue) values ('Low');
SET @ip1 = LAST_INSERT_ID();
insert into ssbi.priorityofissue(PriorityOfIssue) values ('Medium');
SET @ip2 = LAST_INSERT_ID();
insert into ssbi.priorityofissue(PriorityOfIssue) values ('High');
SET @ip3 = LAST_INSERT_ID();
insert into ssbi.priorityofissue(PriorityOfIssue) values ('Urgent');
SET @ip4 = LAST_INSERT_ID();
-- TypeOfProject table inserts
insert into ssbi.typeofissue(TypeOfIssue) values ('Enhancement');
SET @iType1 = LAST_INSERT_ID();
insert into ssbi.typeofissue(TypeOfIssue) values ('Feature');
SET @iType2 = LAST_INSERT_ID();
insert into ssbi.typeofissue(TypeOfIssue) values ('Bug Fix');
SET @iType3 = LAST_INSERT_ID();
*/

/*
CALL addIssue('Make ALL The Featues!', now(), '2015-02-02', 10, 4, 2, 1, 26);
*/

/*CREATE PROCEDURE `addUserToIssue` (in issueid int,in userid int )*/

CALL addUsertoIssue(12, 13);

/*
CALL getAllIssuesForCurrentUser(13);
*/