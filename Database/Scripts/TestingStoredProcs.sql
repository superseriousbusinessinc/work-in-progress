SET @uid = (SELECT User_ID FROM user WHERE UserName = 'Benijermin' LIMIT 0, 1);
DELETE FROM user WHERE User_ID = @uid;

CALL `createUser` (
	'Benjamin',
    'Priyadamkol',
    'benjipriya',
    'Benijermin',
    17,
    11,
    5
);

SELECT LAST_INSERT_ID() AS `User_ID`;

SET @pid = (SELECT Project_ID FROM project WHERE ProjectName = 'Xyno' LIMIT 0, 1);
DELETE FROM project WHERE Project_ID = @pid;

CALL `createProject` (
	'Xyno',
    '2015-03-20',
    16,
    21,
    21,
    6,
    14
);

SELECT LAST_INSERT_ID() AS `Project_ID`;