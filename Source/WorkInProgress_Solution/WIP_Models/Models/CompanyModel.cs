﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WIP_Models.Models
{
    public class CompanyModel : SSBI_Config
    {
        public int Company_ID { get; set; }
        public string CompanyName { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string Region { get; set; }
        public string PostalCode { get; set; }
        public int MaxUsers { get; set; }
        public int CurrentUsers { get; set; }
        public PaymentInformationModel PaymentInformation { get; set; }

        private void setAllProperties(company c)
        {
            Company_ID = c.Company_ID;
            CompanyName = c.CompanyName;
            Address = c.Address;
            City = c.City;
            Region = c.Region;
            PostalCode = c.PostalCode;
            MaxUsers = c.MaxUsers;
            CurrentUsers = c.CurrentUsers;
            PaymentInformationModel pi = new PaymentInformationModel();
            pi.GetByID(c.PaymentInformation_FK);
            PaymentInformation = pi;
        }

        public void GetByID(int id)
        {
            try
            {
                SSBI_Entities db = new SSBI_Entities();
                company obj = db.companies.FirstOrDefault(o => o.Company_ID == id);
                if (obj != null)
                    setAllProperties(obj);
            }
            catch (Exception ex)
            {
                ReportException(ex, "CompanyModel", "GetByID");
            }
        }
    }
}
