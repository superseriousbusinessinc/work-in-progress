﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WIP_Models.Models
{
    public class DepartmentModel : SSBI_Config
    {
        public int Department_ID { get; set; }
        public string DepartmentName { get; set; }

        private void SetAllProperties(department d)
        {
            Department_ID = d.Department_ID;
            DepartmentName = d.DepartmentName;
        }

        public static List<DepartmentModel> GetAll()
        {
            List<DepartmentModel> all = null;
            try
            {
                all = new List<DepartmentModel>();
                SSBI_Entities db = new SSBI_Entities();
                foreach (department d in db.departments)
                {
                    DepartmentModel newTitle = new DepartmentModel();
                    newTitle.SetAllProperties(d);
                    all.Add(newTitle);
                }
            }
            catch (Exception ex)
            {
                ReportException(ex, "DepartmentModel", "GetAll");
            }
            return all;
        }

        public void GetByID(int id)
        {
            try
            {
                SSBI_Entities db = new SSBI_Entities();
                department d = db.departments.FirstOrDefault(de => de.Department_ID == id);
                if (d != null)
                    SetAllProperties(d);
            }
            catch (Exception ex)
            {
                ReportException(ex, "DepartmentModel", "GetByID");
            }
        }
    }
}
