﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WIP_Models.Models
{
    public class IssueModel : SSBI_Config
    {
        public int Issue_ID { get; set; }
        public string IssueName { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime EstimatedTime { get; set; }
        public int Difficulty { get; set; }
        public int IssueStatus_FK { get; set; }
        public Status IssueStatus { get; set; }
        public int IssueType_FK { get; set; }
        public Type IssueType { get; set; }
        public int IssuePriority_FK { get; set; }
        public Priority IssuePriority { get; set; }
        public int Project_FK { get; set; }
        public ProjectModel Project { get; set; }

        public class Type
        {
            public int ID { get; set; }
            public string Name { get; set; }

            private void setAllProperties(typeofissue t)
            {
                ID = t.TypeOfIssue_ID;
                Name = t.TypeOfIssue1;
            }

            public void GetByID(int id)
            {
                try
                {
                    SSBI_Entities db = new SSBI_Entities();
                    typeofissue obj = db.typeofissues.FirstOrDefault(o => o.TypeOfIssue_ID == id);
                    if (obj != null)
                        setAllProperties(obj);
                }
                catch (Exception ex)
                {
                    ReportException(ex, "IssueModel.Type", "GetByID");
                }
            }
        }
        public class Priority
        {
            public int ID { get; set; }
            public string Name { get; set; }

            private void setAllProperties(priorityofissue p)
            {
                ID = p.PriorityOfIssue_ID;
                Name = p.PriorityOfIssue1;
            }

            public void GetByID(int id)
            {
                try
                {
                    SSBI_Entities db = new SSBI_Entities();
                    priorityofissue obj = db.priorityofissues.FirstOrDefault(o => o.PriorityOfIssue_ID == id);
                    if (obj != null)
                        setAllProperties(obj);
                }
                catch (Exception ex)
                {
                    ReportException(ex, "IssueModel.Priority", "GetByID");
                }
            }
        }
        public class Status
        {
            public int ID { get; set; }
            public string Name { get; set; }

            private void setAllProperties(issuestatu s)
            {
                ID = s.IssueStatus_ID;
                Name = s.IssueStatus;
            }

            public void GetByID(int id)
            {
                try
                {
                    SSBI_Entities db = new SSBI_Entities();
                    issuestatu obj = db.issuestatus.FirstOrDefault(o => o.IssueStatus_ID == id);
                    if (obj != null)
                        setAllProperties(obj);
                }
                catch (Exception ex)
                {
                    ReportException(ex, "IssueModel.Status", "GetByID");
                }
            }
        }

        private void setAllProperties(issue i)
        {
            Issue_ID = i.Issue_ID;
            IssueName = i.IssueName;
            DateCreated = i.DateCreated;
            EstimatedTime = i.EstimatedTime;
            Difficulty = i.Difficulty;
            IssueStatus_FK = i.Status_FK;
            Status s = new Status();
            s.GetByID(IssueStatus_FK);
            IssueStatus = s;
            IssueType_FK = i.Type_FK;
            Type t = new Type();
            t.GetByID(IssueType_FK);
            IssueType = t;
            IssuePriority_FK = i.Priority_FK;
            Priority pri = new Priority();
            pri.GetByID(IssuePriority_FK);
            IssuePriority = pri;
            Project_FK = i.Project_FK;
            ProjectModel pm = new ProjectModel();
            pm.GetByID(Project_FK);
            Project = pm;
        }

        public static List<Type> GetAllTypes()
        {
            List<Type> all = null;
            try
            {
                all = new List<Type>();
                SSBI_Entities db = new SSBI_Entities();
                foreach (typeofissue obj in db.typeofissues)
                {
                    Type n = new Type();
                    n.ID = obj.TypeOfIssue_ID;
                    n.Name = obj.TypeOfIssue1;
                    all.Add(n);
                }
            }
            catch (Exception ex)
            {
                ReportException(ex, "IssueModel", "GetAllTypes");
            }
            return all;
        }

        public static List<Priority> GetAllPriorities()
        {
            List<Priority> all = null;
            try
            {
                all = new List<Priority>();
                SSBI_Entities db = new SSBI_Entities();
                foreach (priorityofissue obj in db.priorityofissues)
                {
                    Priority n = new Priority();
                    n.ID = obj.PriorityOfIssue_ID;
                    n.Name = obj.PriorityOfIssue1;
                    all.Add(n);
                }
            }
            catch (Exception ex)
            {
                ReportException(ex, "IssueModel", "GetAllPriorities");
            }
            return all;
        }

        public static List<Status> GetAllStatuses()
        {
            List<Status> all = null;
            try
            {
                all = new List<Status>();
                SSBI_Entities db = new SSBI_Entities();
                foreach (issuestatu obj in db.issuestatus)
                {
                    Status n = new Status();
                    n.ID = obj.IssueStatus_ID;
                    n.Name = obj.IssueStatus;
                    all.Add(n);
                }
            }
            catch (Exception ex)
            {
                ReportException(ex, "IssueModel", "GetAllStatuses");
            }
            return all;
        }

        public static List<IssueModel> GetAllUserIssues(int userID)
        {
            List<IssueModel> all = null;
            try
            {
                all = new List<IssueModel>();
                SSBI_Entities db = new SSBI_Entities();
                foreach (issue i in db.getAllIssuesForCurrentUser(userID))
                {
                    IssueModel im = new IssueModel();
                    im.setAllProperties(i);
                    all.Add(im);
                }
            }
            catch (Exception ex)
            {
                ReportException(ex, "ProjectModel", "GetAllUserProjects");
            }
            return all;
        }

        public static List<IssueModel> GetAllProjectIssues(int projectID)
        {
            List<IssueModel> all = null;
            try
            {
                all = new List<IssueModel>();
                SSBI_Entities db = new SSBI_Entities();
                foreach (issue i in db.getAllIssuesOfProject(projectID))
                {
                    IssueModel im = new IssueModel();
                    im.setAllProperties(i);
                    all.Add(im);
                }
            }
            catch (Exception ex)
            {
                ReportException(ex, "ProjectModel", "GetAllUserProjects");
            }
            return all;
        }

        public static bool CreateIssue(IssueModel im)
        {
            bool success = false;
            try
            {
                SSBI_Entities db = new SSBI_Entities();
                issue i = new issue();
                i.IssueName = im.IssueName;
                i.DateCreated = DateTime.Now;
                i.EstimatedTime = im.EstimatedTime;
                i.Difficulty = im.Difficulty;
                i.Status_FK = im.IssueStatus_FK;
                i.issuestatu = db.issuestatus.FirstOrDefault(s => s.IssueStatus_ID == i.Status_FK);
                i.Type_FK = im.IssueType_FK;
                i.typeofissue = db.typeofissues.FirstOrDefault(t => t.TypeOfIssue_ID == i.Type_FK);
                i.Priority_FK = im.IssuePriority_FK;
                i.priorityofissue = db.priorityofissues.FirstOrDefault(p => p.PriorityOfIssue_ID == i.Project_FK);
                i.Project_FK = im.Project_FK;
                i.project = db.projects.FirstOrDefault(p => p.Project_ID == i.Project_FK);
                db.issues.Add(i);
                db.SaveChanges();
                success = true;
            }
            catch (Exception ex)
            {
                ReportException(ex, "UserModel", "CreateUser");
            }
            return success;
        }

        public void GetByID(int id)
        {
            try
            {
                SSBI_Entities db = new SSBI_Entities();
                issue obj = db.issues.FirstOrDefault(o => o.Issue_ID == id);
                if (obj != null)
                    setAllProperties(obj);
            }
            catch (Exception ex)
            {
                ReportException(ex, "IssueModel", "GetByID");
            }
        }
    }
}
