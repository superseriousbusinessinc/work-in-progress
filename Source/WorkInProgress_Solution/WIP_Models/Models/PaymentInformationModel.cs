﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WIP_Models.Models
{
    public class PaymentInformationModel : SSBI_Config
    {
        public int PaymentInformation_ID { get; set; }
        public string NameOnCard { get; set; }
        public int CardType_FK { get; set; }
        public Type CardType { get; set; }
        public string CardNumber { get; set; }
        public DateTime ExpiryDate { get; set; }
        public string SecurityNumber { get; set; }

        public class Type
        {
            public int ID { get; set; }
            public string Name { get; set; }

            private void setAllProperties(cardtype t)
            {
                ID = t.CardType_ID;
                Name = t.CardType1;
            }

            public void GetByID(int id)
            {
                try
                {
                    SSBI_Entities db = new SSBI_Entities();
                    cardtype obj = db.cardtypes.FirstOrDefault(o => o.CardType_ID == id);
                    if (obj != null)
                        setAllProperties(obj);
                }
                catch (Exception ex)
                {
                    ReportException(ex, "PaymentInformationModel.Type", "GetByID");
                }
            }
        }

        private void setAllProperties(paymentinformation p)
        {
            PaymentInformation_ID = p.PaymentInformation_ID;
            NameOnCard = p.NameOnCard;
            CardType_FK = p.CardType_FK;
            Type t = new Type();
            t.GetByID(CardType_FK);
            CardType = t;
            CardNumber = p.CardNumber;
            ExpiryDate = p.ExpiryDate;
            SecurityNumber = p.SecurityNumber;
        }

        public void GetByID(int id)
        {
            try
            {
                SSBI_Entities db = new SSBI_Entities();
                paymentinformation obj = db.paymentinformations.FirstOrDefault(o => o.PaymentInformation_ID == id);
                if (obj != null)
                    setAllProperties(obj);
            }
            catch (Exception ex)
            {
                ReportException(ex, "PaymentInformationModel", "GetByID");
            }
        }
    }
}
