﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WIP_Models.Models
{
    public class ProjectModel : SSBI_Config
    {
        public int Project_ID { get; set; }
        public string ProjectName { get; set; }
        public DateTime DateStarted { get; set; }
        public DateTime DueDate { get; set; }
        public int ProjectType_FK { get; set; }
        public Type ProjectType { get; set; }
        public int ProjectPriority_FK { get; set; }
        public Priority ProjectPriority { get; set; }
        public int ProjectStatus_FK { get; set; }
        public Status ProjectStatus { get; set; }
        public int Company_FK { get; set; }
        public CompanyModel Company { get; set; }

        public class Type
        {
            public int ID { get; set; }
            public string Name { get; set; }

            private void setAllProperties(typeofproject t)
            {
                ID = t.TypeOfProject_ID;
                Name = t.TypeOfProject1;
            }

            public void GetByID(int id)
            {
                try
                {
                    SSBI_Entities db = new SSBI_Entities();
                    typeofproject obj = db.typeofprojects.FirstOrDefault(o => o.TypeOfProject_ID == id);
                    if (obj != null)
                        setAllProperties(obj);
                }
                catch (Exception ex)
                {
                    ReportException(ex, "ProjectModel.Type", "GetByID");
                }
            }
        }
        public class Priority
        {
            public int ID { get; set; }
            public string Name { get; set; }

            private void setAllProperties(priorityofproject p)
            {
                ID = p.PriorityOfProject_ID;
                Name = p.PriorityOfProject1;
            }

            public void GetByID(int id)
            {
                try
                {
                    SSBI_Entities db = new SSBI_Entities();
                    priorityofproject obj = db.priorityofprojects.FirstOrDefault(o => o.PriorityOfProject_ID == id);
                    if (obj != null)
                        setAllProperties(obj);
                }
                catch (Exception ex)
                {
                    ReportException(ex, "ProjectModel.Priority", "GetByID");
                }
            }
        }
        public class Status
        {
            public int ID { get; set; }
            public string Name { get; set; }

            private void setAllProperties(projectstatu s)
            {
                ID = s.ProjectStatus_ID;
                Name = s.ProjectStatus;
            }

            public void GetByID(int id)
            {
                try
                {
                    SSBI_Entities db = new SSBI_Entities();
                    projectstatu obj = db.projectstatus.FirstOrDefault(o => o.ProjectStatus_ID == id);
                    if (obj != null)
                        setAllProperties(obj);
                }
                catch (Exception ex)
                {
                    ReportException(ex, "ProjectModel.Status", "GetByID");
                }
            }
        }

        private void setAllProperties(project p)
        {
            Project_ID = p.Project_ID;
            ProjectName = p.ProjectName;
            DateStarted = p.DateStarted;
            DueDate = p.DueDate;
            ProjectType_FK = p.Type_FK;
            Type t = new Type();
            t.GetByID(ProjectType_FK);
            ProjectType = t;
            ProjectPriority_FK = p.Priority_FK;
            Priority pri = new Priority();
            pri.GetByID(ProjectPriority_FK);
            ProjectPriority = pri;
            ProjectStatus_FK = p.Status_FK;
            Status s = new Status();
            s.GetByID(ProjectStatus_FK);
            ProjectStatus = s;
            Company_FK = p.Company_FK;
            CompanyModel cm = new CompanyModel();
            cm.GetByID(Company_FK);
            Company = cm;
        }

        public static List<Type> GetAllTypes()
        {
            List<Type> all = null;
            try
            {
                all = new List<Type>();
                SSBI_Entities db = new SSBI_Entities();
                foreach (typeofproject obj in db.typeofprojects)
                {
                    Type n = new Type();
                    n.ID = obj.TypeOfProject_ID;
                    n.Name = obj.TypeOfProject1;
                    all.Add(n);
                }
            }
            catch (Exception ex)
            {
                ReportException(ex, "ProductModel", "GetAllTypes");
            }
            return all;
        }

        public static List<Priority> GetAllPriorities()
        {
            List<Priority> all = null;
            try
            {
                all = new List<Priority>();
                SSBI_Entities db = new SSBI_Entities();
                foreach (priorityofproject obj in db.priorityofprojects)
                {
                    Priority n = new Priority();
                    n.ID = obj.PriorityOfProject_ID;
                    n.Name = obj.PriorityOfProject1;
                    all.Add(n);
                }
            }
            catch (Exception ex)
            {
                ReportException(ex, "ProductModel", "GetAllPriorities");
            }
            return all;
        }

        public static List<Status> GetAllStatuses()
        {
            List<Status> all = null;
            try
            {
                all = new List<Status>();
                SSBI_Entities db = new SSBI_Entities();
                foreach (projectstatu obj in db.projectstatus)
                {
                    Status n = new Status();
                    n.ID = obj.ProjectStatus_ID;
                    n.Name = obj.ProjectStatus;
                    all.Add(n);
                }
            }
            catch (Exception ex)
            {
                ReportException(ex, "ProductModel", "GetAllPriorities");
            }
            return all;
        }

        public static List<ProjectModel> GetAllUserProjects(int userID)
        {
            List<ProjectModel> all = null;
            try
            {
                all = new List<ProjectModel>();
                SSBI_Entities db = new SSBI_Entities();
                foreach (project p in db.getCurrentUserProjects(userID))
                {
                    ProjectModel pm = new ProjectModel();
                    pm.setAllProperties(p);
                    all.Add(pm);
                }
            }
            catch (Exception ex)
            {
                ReportException(ex, "ProjectModel", "GetAllUserProjects");
            }
            return all;
        }

        public static bool CreateProject(ProjectModel project, int userID)
        {
            bool success = false;
            try
            {
                SSBI_Entities db = new SSBI_Entities();
                db.createProject(project.ProjectName, project.DueDate, project.ProjectType_FK, project.ProjectStatus_FK, project.ProjectStatus_FK, project.Company_FK, userID);
                success = true;
            }
            catch (Exception ex)
            {
                ReportException(ex, "UserModel", "CreateUser");
            }
            return success;
        }

        public void GetByID(int id)
        {
            try
            {
                SSBI_Entities db = new SSBI_Entities();
                project obj = db.projects.FirstOrDefault(o => o.Project_ID == id);
                if (obj != null)
                    setAllProperties(obj);
            }
            catch (Exception ex)
            {
                ReportException(ex, "ProjectModel", "GetByID");
            }
        }
    }
}
