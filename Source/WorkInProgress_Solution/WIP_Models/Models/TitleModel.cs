﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WIP_Models.Models
{
    public class TitleModel : SSBI_Config
    {
        public int Title_ID { get; set; }
        public string TitleName { get; set; }

        private void SetAllProperties(title t)
        {
            Title_ID = t.Title_ID;
            TitleName = t.TitleName;
        }

        public static List<TitleModel> GetAll()
        {
            List<TitleModel> all = null;
            try
            {
                all = new List<TitleModel>();
                SSBI_Entities db = new SSBI_Entities();
                foreach (title t in db.titles)
                {
                    TitleModel newTitle = new TitleModel();
                    newTitle.SetAllProperties(t);
                    all.Add(newTitle);
                }
            }
            catch (Exception ex)
            {
                ReportException(ex, "TitleModel", "GetAll");
            }
            return all;
        }

        public void GetByID(int id)
        {
            try
            {
                SSBI_Entities db = new SSBI_Entities();
                title t = db.titles.FirstOrDefault(ti => ti.Title_ID == id);
                if (t != null)
                    SetAllProperties(t);
            }
            catch (Exception ex)
            {
                ReportException(ex, "TitleModel", "GetByID");
            }
        }
    }
}
