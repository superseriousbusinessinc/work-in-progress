﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WIP_Models.Models
{
    public class UserModel : SSBI_Config
    {
        public int User_ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Password { get; set; }
        public string UserName { get; set; }
        public int Title_FK { get; set; }
        public TitleModel Title { get; set; }
        public int Department_FK { get; set; }
        public DepartmentModel Department { get; set; }
        public int Company_FK { get; set; }
        public CompanyModel Company { get; set; }
        public DateTime CreatedOn { get; set; }

        private void SetAllProperties(user u)
        {
            User_ID = u.User_ID;
            FirstName = u.FirstName;
            LastName = u.LastName;
            Password = u.Password;
            UserName = u.UserName;
            Title_FK = u.Title_FK;
            TitleModel tm = new TitleModel();
            tm.GetByID(Title_FK);
            Title = tm;
            Department_FK = u.Department_FK;
            DepartmentModel dm = new DepartmentModel();
            dm.GetByID(Department_FK);
            Department = dm;
            Company_FK = u.Company_FK;
            CompanyModel cm = new CompanyModel();
            cm.GetByID(Company_FK);
            Company = cm;
            CreatedOn = u.CreatedOn;
        }

        public int GetUser(string username, string password)
        {
            int id = -1;
            try
            {
                
                user u = new SSBI_Entities().getUser(username, password).FirstOrDefault();
                if (u != null)
                {
                    SetAllProperties(u);
                    id = User_ID;
                }
            }
            catch (Exception ex)
            {
                ReportException(ex, "UserModel", "GetUser");
            }
            return id;
        }

        public int GetUserByUsername(string username)
        {
            int id = -1;
            try
            {
                user u = new SSBI_Entities().getUserByUsername(username).FirstOrDefault();
                if (u != null)
                {
                    SetAllProperties(u);
                    id = User_ID;
                }
            }
            catch (Exception ex)
            {
                ReportException(ex, "UserModel", "GetUserByUsername");
            }
            return id;
        }

        public static List<UserModel> GetAll()
        {
            List<UserModel> all = null;
            try
            {
                all = new List<UserModel>();
                SSBI_Entities db = new SSBI_Entities();
                foreach(user u in db.users) {
                    UserModel newUser = new UserModel();
                    newUser.SetAllProperties(u);
                    all.Add(newUser);
                }
            }
            catch (Exception ex)
            {
                ReportException(ex, "UserModel", "GetAll");
            }
            return all;
        }

        public static bool CreateUser(UserModel user)
        {
            bool success = false;
            try
            {
                SSBI_Entities db = new SSBI_Entities();
                db.createUser(user.FirstName, user.LastName, user.Password, user.UserName, user.Title_FK, user.Department_FK, user.Company_FK);
                success = true;
            }
            catch (Exception ex)
            {
                ReportException(ex, "UserModel", "CreateUser");
            }
            return success;
        }
    }
}
