﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace WIP_Models
{
    public class SSBI_Config
    {
        public static void ReportException(Exception ex, string className, string methodName)
        {
            if (ex.InnerException != null)
            {
                Debug.WriteLine("ERROR: WIP_Models, " +
                    "Class=" + className + ", " +
                    "Method=" + methodName + ", " +
                    "Inner Exception Message=" + ex.InnerException.Message,
                    EventLogEntryType.Error);
                throw ex.InnerException;
            }
            else
            {
                Debug.WriteLine("ERROR: WIP_Models, " +
                    "Class=" + className + ", " +
                    "Method=" + methodName + ", " +
                    "Inner Exception Message=" + ex.Message,
                    EventLogEntryType.Error);
                throw ex;
            }
        }

        public static byte[] Serializer(Object inObject)
        {
            BinaryFormatter frm = new BinaryFormatter();
            MemoryStream strm = new MemoryStream();
            frm.Serialize(strm, inObject);
            byte[] byteArrayObject = strm.ToArray();
            return byteArrayObject;
        }

        public static Object Deserializer(byte[] byteArrayIn)
        {
            BinaryFormatter frm = new BinaryFormatter();
            MemoryStream strm = new MemoryStream(byteArrayIn);
            Object returnObject = frm.Deserialize(strm);
            return returnObject;
        }
    }
}
