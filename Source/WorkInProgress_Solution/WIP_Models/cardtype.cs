//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WIP_Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class cardtype
    {
        public cardtype()
        {
            this.paymentinformations = new HashSet<paymentinformation>();
        }
    
        public int CardType_ID { get; set; }
        public string CardType1 { get; set; }
    
        public virtual ICollection<paymentinformation> paymentinformations { get; set; }
    }
}
