//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WIP_Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class typeofproject
    {
        public typeofproject()
        {
            this.projects = new HashSet<project>();
        }
    
        public int TypeOfProject_ID { get; set; }
        public string TypeOfProject1 { get; set; }
    
        public virtual ICollection<project> projects { get; set; }
    }
}
