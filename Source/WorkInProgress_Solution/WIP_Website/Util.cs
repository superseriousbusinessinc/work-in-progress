﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.SessionState;

namespace WIP_Website
{
    public static class Util
    {
        public static string TextboxClass = "textbox";
        public static string ButtonClass = "button";

        //Date
        public static string DateFormat = "yyyy-MM-dd";

        public static void ReportException(Exception ex, string className, string methodName)
        {
            if (ex.InnerException != null)
            {
                Debug.WriteLine("ERROR: WIP_Website, " +
                    "Class=" + className + ", " +
                    "Method=" + methodName + ", " +
                    "Inner Exception Message=" + ex.InnerException.Message,
                    EventLogEntryType.Error);
                throw ex.InnerException;
            }
            else
            {
                Debug.WriteLine("ERROR: WIP_Website, " +
                    "Class=" + className + ", " +
                    "Method=" + methodName + ", " +
                    "Inner Exception Message=" + ex.Message,
                    EventLogEntryType.Error);
                throw ex;
            }
        }
    }
}