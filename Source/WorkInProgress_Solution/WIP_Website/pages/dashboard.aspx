﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="dashboard.aspx.cs" Inherits="WIP_Website.pages.dashboard" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <link href="../styles/default.css" rel="stylesheet" />
    <link href="../styles/modal.css" rel="stylesheet" />

    <script src="../scripts/jquery-2.1.3.min.js"></script>
    <script src="../scripts/layout.js"></script>
    <script src="../scripts/dashboard.js"></script>

    <title>Dashboard - Work In Progress</title>
</head>
<body>
    <div id="head" class="headerBar">
        <h1 id="pageTitle">Work In Progress</h1>
        <h6 id="signedInAs" class="subHead">Not signed in.</h6>
    </div>

    <div id="nav" class="navBar">
        <ul>
            <%--<li><a href="javascript:;">Home</a></li>--%>
            <%--<li><a href="javascript:;">User</a></li>--%>
            <li id="navItemMyProjects" class="navItem" onclick="loadUserProjects();">My Projects</li>
            <li id="navItemMyIssues" class="navItem" onclick="loadUserIssues();">My Issues</li>
            <li id="navItemAdminTools" class="navItem" onclick="loadAdminTools();">Admin Tools</li>
            <%--<li><a href="javascript:;">Settings</a></li>--%>
            <li id="navItemLogout" class="navItem" onclick="logout();">Logout</li>
        </ul>
    </div>

    <div id="main" class="mainContent">
        <%--Home Stuff?--%>
        <%--        <table class="homeTable">
            <tr>
                <td colspan="2">
                    <div class="homePanels">
                        <div class="titleDiv">
                            <h4>News</h4>
                        </div>
                        <ul class="homeProjectList">
                            <li><a href="javascript:;">News Item 1</a></li>
                            <li><a href="javascript:;">News Item 2</a></li>
                            <li><a href="javascript:;">News Item 3</a></li>
                            <li><a href="javascript:;">News Item 4</a></li>
                            <li><a href="javascript:;">News Item 5</a></li>
                        </ul>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="homePanels">
                        <div class="titleDiv">
                            <h4>My Projects</h4>
                        </div>
                        <ul class="homeProjectList">
                            <li><a href="javascript:;">Project 1</a></li>
                            <li><a href="javascript:;">Project 2</a></li>
                            <li><a href="javascript:;">Project 3</a></li>
                            <li><a href="javascript:;">Project 4</a></li>
                            <li><a href="javascript:;">Project 5</a></li>
                        </ul>
                    </div>
                </td>
            </tr>
        </table>--%>

        <%---------------------------------- PROJECTS ----------------------------------%>
        <div id="projectsDiv" class="subDashboard" style="display: none;">
            <div id="projectsTitle" class="subTitle"></div>
            <div id="projectsOptionsBar" class="hOptionsBar">
                <div class="hOption"><a href="javascript:;" onclick="loadUserProjects();">View My Projects</a></div>
                <div class="hOption"><a href="javascript:;" onclick="loadCreateProjectForm();">Create Project</a></div>
            </div>
            <div id="projectsDiv_hiddenMsg" style="display: none;"></div>
            <div id="projectsGrid" class="grid" style="display: none;"></div>
            <div id="projectView" style="display: none;"></div>
        </div>
        <%---------------------------------- END PROJECTS ----------------------------------%>

        <%---------------------------------- ISSUES ----------------------------------%>
        <div id="issuesDiv" class="subDashboard" style="display: none;">
            <div id="issuesTitle" class="subTitle"></div>
            <div id="issuesOptionsBar" class="hOptionsBar">
                <div class="hOption"><a href="javascript:;" onclick="loadUserIssues();">View My Issues</a></div>
            </div>
            <div id="issuesDiv_hiddenMsg" style="display: none;"></div>
            <div id="issuesGrid" class="grid" style="display: none;"></div>
            <div id="issueView" style="display: none;"></div>
        </div>
        <%---------------------------------- END ISSUES ----------------------------------%>

        <%---------------------------------- ADMIN TOOLS ----------------------------------%>
        <div id="adminToolsDiv" class="subDashboard" style="display: none;">
            <div id="adminToolsTitle" class="subTitle">Administrator Tools</div>
            <div id="adminToolsOptionsBar" class="hOptionsBar">
                <div class="hOption"><a href="javascript:;" onclick="loadCreateUserForm();">Create User</a></div>
            </div>
            <div id="formDiv"></div>
        </div>
        <%---------------------------------- END ADMIN TOOLS ----------------------------------%>

        <div id="mShadow" class="shadowScreen" onclick="hideModal();" style="display: none; opacity: 0.0;"></div>

        <div id="formModal" class="modal" style="display: none; opacity: 0.0;">
            <div id="mTop" class="mTop">
                <div id="mTitle" class="mTitle"></div>
                <button id="btnMX" class="mBtnX" onclick="hideModal();">X</button>
            </div>
            <div id="mBody" class="mBody"></div>
            <div id="mBottom" class="mBottom" style="text-align: right;">
                <div id="mHiddenMsg" class="mHiddenMessage" style="display: none;"></div>
                <button id="btnMClose" class="mBtn" onclick="hideModal();">Close</button>
            </div>
        </div>
    </div>
</body>
</html>
