﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using WIP_Website;
using WIP_Models.Models;
using System.Web.Services;

namespace WIP_Website.pages
{
    public partial class dashboard : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        /* Purpose: Logs the user out
         * Require: The current Http session.
         * Returns: bool - true: success, false: fail
         */
        [WebMethod]
        public static bool Logout()
        {
            bool success = false;
            try
            {
                if (HttpContext.Current.Session["User_ID"] != null)
                {
                    HttpContext.Current.Session.RemoveAll();
                    success = true;
                }
            }
            catch (Exception ex)
            {
                Util.ReportException(ex, "dashboard.aspx.cs", "Logout");
            }
            return success;
        }

        [WebMethod]
        public static object[] LoadUserProjects()
        {
            object[] retObj = null;
            try
            {
                if (HttpContext.Current.Session["User_ID"] != null)
                {
                    int userID = (int)HttpContext.Current.Session["User_ID"];
                    List<ProjectModel> all = ProjectModel.GetAllUserProjects(userID);
                    if (all != null)
                    {
                        List<object> allObjs = new List<object>();
                        foreach (ProjectModel pm in all)
                        {
                            allObjs.Add(new
                            {
                                projectID = pm.Project_ID,
                                projectName = pm.ProjectName,
                                dateStarted = pm.DateStarted.ToString(Util.DateFormat),
                                dueDate = pm.DueDate.ToString(Util.DateFormat),
                                projectType = pm.ProjectType,
                                projectPriority = pm.ProjectPriority,
                                projectStatus = pm.ProjectStatus,
                                company_FK = pm.Company_FK
                            });
                        }
                        retObj = allObjs.ToArray();
                    }
                }
            }
            catch (Exception ex)
            {
                Util.ReportException(ex, "dashboard.aspx.cs", "LoadUserProjects");
            }
            return retObj;
        }

        [WebMethod]
        public static object[] LoadUserIssues()
        {
            object[] retObj = null;
            try
            {
                if (HttpContext.Current.Session["User_ID"] != null)
                {
                    int userID = (int)HttpContext.Current.Session["User_ID"];
                    List<IssueModel> all = IssueModel.GetAllUserIssues(userID);
                    if (all != null)
                    {
                        List<object> allObjs = new List<object>();
                        foreach (IssueModel im in all)
                        {
                            allObjs.Add(new
                            {
                                issueID = im.Issue_ID,
                                issueName = im.IssueName,
                                dateCreated = im.DateCreated.ToString(Util.DateFormat),
                                estimatedTime = im.EstimatedTime.ToString(Util.DateFormat),
                                difficulty = im.Difficulty,
                                issueStatus = im.IssueStatus,
                                issueType = im.IssueType,
                                issuePriority = im.IssuePriority,
                                project = im.Project
                            });
                        }
                        retObj = allObjs.ToArray();
                    }
                }
            }
            catch (Exception ex)
            {
                Util.ReportException(ex, "dashboard.aspx.cs", "LoadUserIssues");
            }
            return retObj;
        }

        [WebMethod]
        public static object[] LoadProjectIssues(string projectID)
        {
            object[] retObj = null;
            try
            {
                List<IssueModel> all = IssueModel.GetAllProjectIssues(int.Parse(projectID));
                if (all != null)
                {
                    List<object> allObjs = new List<object>();
                    foreach (IssueModel im in all)
                    {
                        allObjs.Add(new
                        {
                            issueID = im.Issue_ID,
                            issueName = im.IssueName,
                            dateCreated = im.DateCreated.ToString(Util.DateFormat),
                            estimatedTime = im.EstimatedTime.ToString(Util.DateFormat),
                            difficulty = im.Difficulty,
                            issueStatus = im.IssueStatus,
                            issueType = im.IssueType,
                            issuePriority = im.IssuePriority,
                            project = im.Project
                        });
                    }
                    retObj = allObjs.ToArray();
                }
            }
            catch (Exception ex)
            {
                Util.ReportException(ex, "dashboard.aspx.cs", "LoadProjectIssues");
            }
            return retObj;
        }
    }
}