﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="issueCreate.aspx.cs" Inherits="WIP_Website.pages.forms.issueCreate" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <script>
        $(document).ready(initIssueCreate);
        var issueFormProjectID = null;
        function initIssueCreate() {
            $("#createIssueForm").submit(function (e) {
                e.preventDefault();
            });
            loadIssueCreateOptions();
        }

        function loadIssueCreateOptions() {
            $.ajax({
                url: "forms/issueCreate.aspx/GetIssueOptions",
                type: "POST",
                contentType: "application/json",
                dataType: "JSON",
                timeout: 60000,
                success: function (result) {
                    console.log("issueCreate.loadOptions: SUCCESS");
                    var option = null;
                    for (var i = 0; i < result.d.types.length; i++) {
                        option = document.createElement("option");
                        option.value = result.d.types[i].ID;
                        option.innerHTML = result.d.types[i].Name;
                        document.getElementById("cmbIssueType").appendChild(option);
                    }
                    for (var i = 0; i < result.d.priorities.length; i++) {
                        option = document.createElement("option");
                        option.value = result.d.priorities[i].ID;
                        option.innerHTML = result.d.priorities[i].Name;
                        document.getElementById("cmbIssuePriority").appendChild(option);
                    }
                    for (var i = 0; i < result.d.statuses.length; i++) {
                        option = document.createElement("option");
                        option.value = result.d.statuses[i].ID;
                        option.innerHTML = result.d.statuses[i].Name;
                        document.getElementById("cmbIssueStatus").appendChild(option);
                    }
                },
                error: function (xhr, status) {
                    console.log("issueCreate.loadOptions: ERROR");
                    showError("mHiddenMsg", xhr, status);
                }
            });
        }

        function clearIssueCreateForm() {
            $("#txtIssueName").val('');
            $("#txtEstimatedTime").val('');
        }

        function loadProjectForIssueForm(project) {
            issueFormProjectID = project.projectID;
            $("#issueFormTitle").html('Create Issue for "' + project.projectName + '"');
        }

        //CREATE
        function createIssue(projectID) {
            $.ajax({
                url: "forms/issueCreate.aspx/CreateIssue",
                data: JSON.stringify({
                    issueName: $("#txtIssueName").val(),
                    estimatedTime: $("#txtEstimatedTime").val(),
                    difficulty: $("#txtDifficulty").val(),
                    type: $("#cmbIssueType").val(),
                    priority: $("#cmbIssuePriority").val(),
                    status: $("#cmbIssueStatus").val(),
                    projectID: issueFormProjectID
                }),
                type: "POST",
                contentType: "application/json",
                dataType: "JSON",
                timeout: 60000,
                success: function (result) {
                    console.log("createIssue: SUCCESS");
                    console.log(result.d);
                    if (result.d) {
                        createIssueSuccess(result.d);
                    } else {
                        createIssueFailure(result.d);
                    }
                },
                error: function (xhr, status) {
                    console.log("createIssue: ERROR");
                    showError("issueForm_hiddenMsg", xhr, status);
                }
            });
        }

        function createIssueSuccess(project) {
            $("#mHiddenMsg").css("display", "inline");
            $("#mHiddenMsg").css("color", "green");
            $("#mHiddenMsg").html("Issue was successfully created!");
            clearIssueCreateForm();
        }

        function createIssueFailure(project) {
            $("#mHiddenMsg").css("display", "inline");
            $("#mHiddenMsg").css("color", "red");
            $("#mHiddenMsg").html("Creating issue failed!");
        }
    </script>
    <title></title>
</head>
<body>
    <div style="text-align: center;">
        <button id="btnClearForm_Issue" class="button" onclick="clearIssueCreateForm();">Clear Form</button>
    </div>
    <form id="createIssueForm" runat="server" class="form">
        <table>
            <tr>
                <td id="issueFormTitle" colspan="2" class="formTitle">Create Issue
                </td>
            </tr>
            <tr>
                <td>
                    <label class="label" for="txtIssueName">Issue Name</label>
                </td>
                <td>
                    <asp:TextBox ID="txtIssueName" CssClass="textbox" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <label class="label" for="txtEstimatedTime">Estimated Completion</label>
                </td>
                <td>
                    <asp:TextBox ID="txtEstimatedTime" CssClass="textbox" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <label class="label" for="txtDifficulty">Difficulty</label>
                </td>
                <td>
                    <asp:TextBox ID="txtDifficulty" CssClass="textbox" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <label class="label" for="cmbIssueType">Type</label>
                </td>
                <td>
                    <select id="cmbIssueType" class="combobox"></select>
                </td>
            </tr>
            <tr>
                <td>
                    <label class="label" for="cmbIssuePriority">Priority</label>
                </td>
                <td>
                    <select id="cmbIssuePriority" class="combobox"></select>
                </td>
            </tr>
            <tr>
                <td>
                    <label class="label" for="cmbIssueStatus">Status</label>
                </td>
                <td>
                    <select id="cmbIssueStatus" class="combobox"></select>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: center;">
                    <asp:Button ID="btnCreateIssue" Text="Create Issue" CssClass="button" runat="server" />
                </td>
            </tr>
        </table>
        <asp:RequiredFieldValidator ControlToValidate="txtIssueName" ErrorMessage="Issue name is required." ForeColor="Red" runat="server" />
        <br />
        <asp:RequiredFieldValidator ControlToValidate="txtEstimatedTime" ErrorMessage="Estimated Completion is required." ForeColor="Red" runat="server" />
        <br />
        <asp:RequiredFieldValidator ControlToValidate="txtDifficulty" ErrorMessage="Difficulty is required." ForeColor="Red" runat="server" />
        <br />
        <asp:CompareValidator ControlToValidate="txtDifficulty" Type="Integer" Operator="DataTypeCheck" ErrorMessage="Difficulty must be an integer greater than or equal to 0." ForeColor="Red" runat="server" />
    </form>
</body>
</html>
