﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

using WIP_Website;
using WIP_Models.Models;

namespace WIP_Website.pages.forms
{
    public partial class issueCreate : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        /* Purpose: 
         * Require: 
         * Returns: {int user_ID, string username} OR null
         */
        [WebMethod]
        public static bool CreateIssue(string issueName, string estimatedTime, string difficulty, string type, string priority, string status, string projectID)
        {
            bool success = false;
            try
            {
                IssueModel im = new IssueModel();
                im.IssueName = issueName;
                im.EstimatedTime = Convert.ToDateTime(estimatedTime);
                im.Difficulty = int.Parse(difficulty);
                im.IssueType_FK = int.Parse(type);
                im.IssuePriority_FK = int.Parse(priority);
                im.IssueStatus_FK = int.Parse(status);
                im.Project_FK = int.Parse(projectID);
                success = IssueModel.CreateIssue(im);
            }
            catch (Exception ex)
            {
                Util.ReportException(ex, "userCreate.aspx.cs", "CreateUser");
            }
            return success;
        }

        /* Purpose: 
         * Require: 
         * Returns: {int user_ID, string username} OR null
         */
        [WebMethod]
        public static object GetIssueOptions()
        {
            object retObj = null;
            try
            {
                retObj = new
                {
                    types = IssueModel.GetAllTypes().ToArray(),
                    priorities = IssueModel.GetAllPriorities().ToArray(),
                    statuses = IssueModel.GetAllStatuses().ToArray()
                };
            }
            catch (Exception ex)
            {
                Util.ReportException(ex, "projectCreate.aspx.cs", "GetProjectOptions");
            }
            return retObj;
        }
    }
}