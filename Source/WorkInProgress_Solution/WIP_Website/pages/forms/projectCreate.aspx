﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="projectCreate.aspx.cs" Inherits="WIP_Website.pages.forms.projectCreate" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <script>
        $(document).ready(initProjectCreate);

        function initProjectCreate() {
            $("#createProjectForm").submit(function (e) {
                e.preventDefault();
            });
            loadProjectCreateOptions();
        }

        function loadProjectCreateOptions() {
            $.ajax({
                url: "forms/projectCreate.aspx/GetProjectOptions",
                type: "POST",
                contentType: "application/json",
                dataType: "JSON",
                timeout: 60000,
                success: function (result) {
                    console.log("populateProjectComboBoxes: SUCCESS");
                    var option = null;
                    for (var i = 0; i < result.d.types.length; i++) {
                        option = document.createElement("option");
                        option.value = result.d.types[i].ID;
                        option.innerHTML = result.d.types[i].Name;
                        document.getElementById("cmbType").appendChild(option);
                    }
                    for (var i = 0; i < result.d.priorities.length; i++) {
                        option = document.createElement("option");
                        option.value = result.d.priorities[i].ID;
                        option.innerHTML = result.d.priorities[i].Name;
                        document.getElementById("cmbPriority").appendChild(option);
                    }
                    for (var i = 0; i < result.d.statuses.length; i++) {
                        option = document.createElement("option");
                        option.value = result.d.statuses[i].ID;
                        option.innerHTML = result.d.statuses[i].Name;
                        document.getElementById("cmbStatus").appendChild(option);
                    }
                },
                error: function (xhr, status) {
                    console.log("populateProjectComboBoxes: ERROR");
                    showError("mHiddenMsg", xhr, status);
                }
            });
        }

        function setProjectCreateToEdit(project) {
            $("#txtProjectName").val(project.projectName);
            $("#txtDateStarted").val(project.dateStarted);
            $("#txtDueDate").val(project.dueDate);
            $("#cmbType").val(project.projectType.Name);
            $("#cmbPriority").val(project.projectPriority.Name);
            $("#cmbStatus").val(project.projectStatus.Name);

            $("#projectFormTitle").html('Edit Project: "' + project.projectName + '"');
        }

        function clearProjectCreateForm() {
            $("#txtProjectName").val('');
            $("#txtDateStarted").val('');
            $("#txtDueDate").val('');
        }

        //CREATE
        function createProject() {
            $.ajax({
                url: "forms/projectCreate.aspx/CreateProject",
                data: JSON.stringify({
                    projectName: $("#txtProjectName").val(),
                    dateStarted: $("#txtDateStarted").val(),
                    dueDate: $("#txtDueDate").val(),
                    type: $("#cmbType").val(),
                    priority: $("#cmbPriority").val(),
                    status: $("#cmbStatus").val()
                }),
                type: "POST",
                contentType: "application/json",
                dataType: "JSON",
                timeout: 60000,
                success: function (result) {
                    console.log("createProject: SUCCESS");
                    console.log(result.d);
                    if (result.d) {
                        createProjectSuccess(result.d);
                    } else {
                        createProjectFailure(result.d);
                    }
                },
                error: function (xhr, status) {
                    console.log("createProject: ERROR");
                    showError("mHiddenMsg", xhr, status);
                }
            });
        }

        function createProjectSuccess(project) {
            $("#mHiddenMsg").css("display", "inline");
            $("#mHiddenMsg").css("color", "green");
            $("#mHiddenMsg").html("Project was successfully created!");
            clearProjectCreateForm();
        }

        function createProjectFailure(project) {
            $("#mHiddenMsg").css("display", "inline");
            $("#mHiddenMsg").css("color", "red");
            $("#mHiddenMsg").html("Creating project failed!");
        }
    </script>
    <title></title>
</head>
<body>
    <div style="text-align: center;">
        <button id="btnClearForm_CreateProject" class="button" onclick="clearProjectCreateForm();">Clear Form</button>
    </div>
    <form id="createProjectForm" runat="server" class="form">
        <table>
            <tr>
                <td id="projectFormTitle" colspan="2" class="formTitle">Create Project
                </td>
            </tr>
            <tr>
                <td>
                    <label class="label" for="txtProjectName">Project Name</label>
                </td>
                <td>
                    <asp:TextBox ID="txtProjectName" CssClass="textbox" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <label class="label" for="txtDateStarted">Date Started</label>
                </td>
                <td>
                    <asp:TextBox ID="txtDateStarted" CssClass="textbox" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <label class="label" for="txtDueDate">Due Date</label>
                </td>
                <td>
                    <asp:TextBox ID="txtDueDate" CssClass="textbox" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <label class="label" for="cmbType">Type</label>
                </td>
                <td>
                    <select id="cmbType" class="combobox"></select>
                </td>
            </tr>
            <tr>
                <td>
                    <label class="label" for="cmbPriority">Priority</label>
                </td>
                <td>
                    <select id="cmbPriority" class="combobox"></select>
                </td>
            </tr>
            <tr>
                <td>
                    <label class="label" for="cmbStatus">Status</label>
                </td>
                <td>
                    <select id="cmbStatus" class="combobox"></select>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: center;">
                    <asp:Button ID="btnCreateProject" Text="Create Project" CssClass="button" runat="server" />
                </td>
            </tr>
        </table>
        <asp:RequiredFieldValidator Display="Dynamic" ControlToValidate="txtProjectName" ErrorMessage="Project name is required." ForeColor="Red" runat="server" />
        <br />
        <asp:RequiredFieldValidator Display="Dynamic" ControlToValidate="txtDateStarted" ErrorMessage="Date started is required." ForeColor="Red" runat="server" />
        <br />
        <asp:RequiredFieldValidator Display="Dynamic" ControlToValidate="txtDueDate" ErrorMessage="Expected completion date is required." ForeColor="Red" runat="server" />
    </form>
</body>
</html>
