﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

using WIP_Website;
using WIP_Models.Models;

namespace WIP_Website.pages.forms
{
    public partial class projectCreate : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        /* Purpose: 
         * Require: 
         * Returns: {int user_ID, string username} OR null
         */
        [WebMethod]
        public static bool CreateProject(string projectName, string dateStarted, string dueDate, string type, string priority, string status)
        {
            bool success = false;
            try
            {
                ProjectModel pModel = new ProjectModel();
                pModel.ProjectName = projectName;
                pModel.DateStarted = Convert.ToDateTime(dateStarted);
                pModel.DueDate = Convert.ToDateTime(dueDate);
                pModel.ProjectType_FK = int.Parse(type);
                pModel.ProjectPriority_FK = int.Parse(priority);
                pModel.ProjectStatus_FK = int.Parse(status);
                pModel.Company_FK = (int)HttpContext.Current.Session["Company_ID"];
                success = ProjectModel.CreateProject(pModel, (int)HttpContext.Current.Session["User_ID"]);
            }
            catch (Exception ex)
            {
                Util.ReportException(ex, "userCreate.aspx.cs", "CreateUser");
            }
            return success;
        }

        /* Purpose: 
         * Require: 
         * Returns: {int user_ID, string username} OR null
         */
        [WebMethod]
        public static object GetProjectOptions()
        {
            object retObj = null;
            try
            {
                retObj = new
                {
                    types = ProjectModel.GetAllTypes().ToArray(),
                    priorities = ProjectModel.GetAllPriorities().ToArray(),
                    statuses = ProjectModel.GetAllStatuses().ToArray()
                };
            }
            catch (Exception ex)
            {
                Util.ReportException(ex, "projectCreate.aspx.cs", "GetProjectOptions");
            }
            return retObj;
        }
    }
}