﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="userCreate.aspx.cs" Inherits="WIP_Website.pages.forms.userCreate" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <script>
        $(document).ready(initUserCreate);
        function initUserCreate() {
            $("#createUserForm").submit(function (e) {
                e.preventDefault();
            });
            loadUserCreateOptions();
        }

        function loadUserCreateOptions() {
            $.ajax({
                url: "forms/userCreate.aspx/GetUserOptions",
                type: "POST",
                contentType: "application/json",
                dataType: "JSON",
                timeout: 60000,
                success: function (result) {
                    console.log("populateUserComboBoxes: SUCCESS");
                    var option = null;
                    for (var i = 0; i < result.d.titles.length; i++) {
                        option = document.createElement("option");
                        option.value = result.d.titles[i].Title_ID;
                        option.innerHTML = result.d.titles[i].TitleName;
                        document.getElementById("cmbTitle").appendChild(option);
                    }
                    for (i = 0; i < result.d.departments.length; i++) {
                        option = document.createElement("option");
                        option.value = result.d.departments[i].Department_ID;
                        option.innerHTML = result.d.departments[i].DepartmentName;
                        document.getElementById("cmbDepartment").appendChild(option);
                    }
                },
                error: function (xhr, status) {
                    console.log("populateUserComboBoxes: ERROR");
                    showError("mHiddenMsg", xhr, status);
                }
            });
        }

        function clearUserCreateForm() {
            $("#txtFirstName").val('');
            $("#txtLastName").val('');
            $("#txtUsername").val('');
            $("#txtPassword").val('');
            $("#txtConfirmPassword").val('');
        }

        //CREATE
        function createUser() {
            $.ajax({
                url: "forms/userCreate.aspx/CreateUser",
                data: JSON.stringify({
                    firstName: $("#txtFirstName").val(),
                    lastName: $("#txtLastName").val(),
                    password: $("#txtPassword").val(),
                    userName: $("#txtUsername").val(),
                    titleID: $("#cmbTitle").val(),
                    departmentID: $("#cmbDepartment").val()
                }),
                type: "POST",
                contentType: "application/json",
                dataType: "JSON",
                timeout: 60000,
                success: function (result) {
                    console.log("createUser: SUCCESS");
                    console.log(result.d);
                    if (result.d) {
                        createUserSuccess(result.d);
                    } else {
                        createUserFailure(result.d);
                    }
                },
                error: function (xhr, status) {
                    console.log("createUser: ERROR");
                    showError("mHiddenMsg", xhr, status);
                }
            });
        }

        function createUserSuccess(user) {
            $("#mHiddenMsg").css("display", "inline");
            $("#mHiddenMsg").css("color", "green");
            $("#mHiddenMsg").html("User was successfully created!");
            clearUserCreateForm();
        }

        function createUserFailure(user) {
            $("#mHiddenMsg").css("display", "inline");
            $("#mHiddenMsg").css("color", "red");
            $("#mHiddenMsg").html("Creating user failed!");
        }
    </script>
    <title></title>
</head>
<body>
    <div style="text-align: center;">
        <button id="btnClearForm_CreateUser" class="button" onclick="clearUserCreateForm();">Clear Form</button>
    </div>
    <form id="createUserForm" runat="server" class="form">
        <table>
            <tr>
                <td id="userFormTitle" colspan="2" class="formTitle">Create User
                </td>
            </tr>
            <tr>
                <td>
                    <label class="label" for="cmbTitle">Title</label>
                </td>
                <td>
                    <select id="cmbTitle" class="combobox"></select>
                </td>
            </tr>
            <tr>
                <td>
                    <label class="label" for="txtFirstName">First Name</label>
                </td>
                <td>
                    <asp:TextBox ID="txtFirstName" CssClass="textbox" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <label class="label" for="txtLastName">Last Name</label>
                </td>
                <td>
                    <asp:TextBox ID="txtLastName" CssClass="textbox" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <label class="label" for="txtUsername">Username</label>
                </td>
                <td>
                    <asp:TextBox ID="txtUsername" CssClass="textbox" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <label class="label" for="txtPassword">Password</label>
                </td>
                <td>
                    <asp:TextBox ID="txtPassword" TextMode="Password" CssClass="textbox" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <label class="label" for="txtConfirmPassword">Confirm Password</label>
                </td>
                <td>
                    <asp:TextBox ID="txtConfirmPassword" TextMode="Password" CssClass="textbox" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <label class="label" for="cmbDepartment">Department</label>
                </td>
                <td>
                    <select id="cmbDepartment" class="combobox"></select>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="text-align: center;">
                    <asp:Button ID="btnCreateUser" Text="Create User" CssClass="button" runat="server" />
                </td>
            </tr>
        </table>
        <asp:RequiredFieldValidator ControlToValidate="txtFirstName" ErrorMessage="First name is required." ForeColor="Red" runat="server" />
        <br />
        <asp:RequiredFieldValidator ControlToValidate="txtLastname" ErrorMessage="Last name is required." ForeColor="Red" runat="server" />
        <br />
        <asp:RequiredFieldValidator ControlToValidate="txtUsername" ErrorMessage="Username is required." ForeColor="Red" runat="server" />
        <br />
        <asp:RequiredFieldValidator ControlToValidate="txtPassword" ErrorMessage="Password is required." ForeColor="Red" runat="server" />
        <br />
        <asp:RequiredFieldValidator ControlToValidate="txtConfirmPassword" ErrorMessage="Confirm Password is required." ForeColor="Red" runat="server" />
    </form>
</body>
</html>
