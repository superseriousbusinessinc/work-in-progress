﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

using WIP_Website;
using WIP_Models.Models;

namespace WIP_Website.pages.forms
{
    public partial class userCreate : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        /* Purpose: 
         * Require: 
         * Returns: {int user_ID, string username} OR null
         */
        [WebMethod]
        public static bool CreateUser(string firstName, string lastName, string password, string userName, string titleID, string departmentID)
        {
            bool success = false;
            try
            {
                UserModel uModel = new UserModel();
                if (uModel.GetUserByUsername(userName) == -1)
                {
                    uModel.FirstName = firstName;
                    uModel.LastName = lastName;
                    uModel.Password = password;
                    uModel.UserName = userName;
                    uModel.Title_FK = int.Parse(titleID);
                    uModel.Department_FK = int.Parse(departmentID);
                    uModel.Company_FK = (int)HttpContext.Current.Session["Company_ID"];
                    success = UserModel.CreateUser(uModel);
                }
            }
            catch (Exception ex)
            {
                Util.ReportException(ex, "userCreate.aspx.cs", "CreateUser");
            }
            return success;
        }

        /* Purpose: 
         * Require: 
         * Returns: {int user_ID, string username} OR null
         */
        [WebMethod]
        public static object GetUserOptions()
        {
            object retObj = null;
            try
            {
                retObj = new
                {
                    titles = TitleModel.GetAll().ToArray(),
                    departments = DepartmentModel.GetAll().ToArray()
                };
            }
            catch (Exception ex)
            {
                Util.ReportException(ex, "userCreate.aspx.cs", "GetUserOptions");
            }
            return retObj;
        }
    }
}