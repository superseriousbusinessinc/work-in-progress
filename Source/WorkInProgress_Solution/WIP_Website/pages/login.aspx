﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="login.aspx.cs" Inherits="WIP_Website.pages.login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <link href="../styles/default.css" rel="stylesheet" />

    <script src="../scripts/jquery-2.1.3.min.js"></script>
    <script src="../scripts/layout.js"></script>
    <script src="../scripts/login.js"></script>

    <title>Work In Progress - Login</title>
</head>
<body>
    <div id="head" class="headerBar">
        <h1>Work In Progress</h1>
    </div>

    <div id="nav" class="navBar">
        <ul>
            <li class="navItem selected">Login</li>
        </ul>
    </div>

    <div id="main" class="mainContent">
        <form id="loginForm" class="form" runat="server" onsubmit="login();">
            <table>
                <tr>
                    <td id="loginFormTitle" colspan="2" class="formTitle">
                        Enter Login Info
                    </td>
                </tr>
                <tr>
                    <td>
                        <label class="label" for="txtUsername">Username</label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtUsername" CssClass="textbox" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <label class="label" for="txtPassword">Password</label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtPassword" TextMode="Password" CssClass="textbox" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="text-align: center;">
                        <asp:Button ID="btnLogin" Text="Login" CssClass="textbox" runat="server" />
                    </td>
                </tr>
            </table>
            <div style="margin-bottom: 20px">
                <div>
                    <asp:RequiredFieldValidator ControlToValidate="txtUsername" ErrorMessage="Username is required." ForeColor="Red" runat="server" />
                </div>
                <div>
                    <asp:RequiredFieldValidator ControlToValidate="txtPassword" ErrorMessage="Password is required." ForeColor="Red" runat="server" />
                </div>
            </div>
        </form>
        <div id="hiddenMsg" style="display: none;"></div>
    </div>
</body>
</html>
