﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

using WIP_Website;
using WIP_Models.Models;
using System.Web.SessionState;

namespace WIP_Website.pages
{
    public partial class login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        /* Purpose: Checks to find a user that matches the username and password. Returns null if nothing valid is found.
         * Require: string username, string password
         * Returns: {int user_ID, string username} OR null
         */
        [WebMethod]
        public static object Login(string username, string password)
        {
            object retObj = null;
            try
            {
                UserModel user = new UserModel();
                if (user.GetUser(username, password) != -1)
                {
                    HttpContext.Current.Session["User_ID"] = user.User_ID;
                    HttpContext.Current.Session["UserName"] = user.UserName;
                    HttpContext.Current.Session["Company_ID"] = user.Company.Company_ID;
                    HttpContext.Current.Session["CompanyName"] = user.Company.CompanyName;
                    retObj = GetSession();
                }
            }
            catch (Exception ex)
            {
                Util.ReportException(ex, "login.aspx.cs", "Login");
            }
            return retObj;
        }

        /* Purpose: 
         * Require: 
         * Returns: {int user_ID, string username} OR null
         */
        [WebMethod]
        public static object GetSession()
        {
            object retObj = null;
            try
            {
                if (HttpContext.Current.Session["User_ID"] != null)
                {
                    retObj = new
                    {
                        user_ID = HttpContext.Current.Session["User_ID"],
                        username = HttpContext.Current.Session["UserName"],
                        company_ID = HttpContext.Current.Session["Company_ID"],
                        companyName = HttpContext.Current.Session["CompanyName"]
                    };
                }
            }
            catch (Exception ex)
            {
                Util.ReportException(ex, "login.aspx.cs", "GetSession");
            }
            return retObj;
        }
    }
}