﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="issueView.aspx.cs" Inherits="WIP_Website.pages.issuesView" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <script>
        var issueInfo = null;

        function populateIssueView(issue) {
            issueInfo = issue;
            $("#issueViewTitle").html("Issue: " + issue.issueName);
            $("#lblIssueID").html(issue.issueID);
            $("#lblIssueName").html(issue.issueName);
            $("#lblDateCreated").html(issue.dateCreated);
            $("#lblEstimatedTime").html(issue.estimatedTime);
            $("#lblDifficulty").html(issue.difficulty);
            $("#lblIssueType").html(issue.issueType.Name);
            $("#lblIssuePriority").html(issue.issuePriority.Name);
            $("#lblIssueStatus").html(issue.issueStatus.Name);
            $("#lblIssueProject").html(issue.project.ProjectName);
        }
    </script>

    <title></title>
</head>
<body>
    <div class="viewOptions">
        <button id="btnEditIssue" class="button">Edit</button>
        <button id="btnManageIssueUsers" class="button">Manage Users</button>
    </div>
    <table id="viewIssue" class="form">
        <tr>
            <td class="formTitle" colspan="2" id="issueViewTitle">Issue: 
            </td>
        </tr>
        <tr>
            <td>
                <label class="label" for="lblIssueID">Issue ID</label>
            </td>
            <td>
                <label class="label" id="lblIssueID"></label>
            </td>
        </tr>
        <tr>
            <td>
                <label class="label" for="lblIssueName">Issue Name</label>
            </td>
            <td>
                <label class="label" id="lblIssueName"></label>
            </td>
        </tr>
        <tr>
            <td>
                <label class="label" for="lblDateCreated">Date Created</label>
            </td>
            <td>
                <label class="label" id="lblDateCreated"></label>
            </td>
        </tr>
        <tr>
            <td>
                <label class="label" for="lblEstimatedTime">Estimated Time</label>
            </td>
            <td>
                <label class="label" id="lblEstimatedTime"></label>
            </td>
        </tr>
        <tr>
            <td>
                <label class="label" for="lblDifficulty">Difficulty</label>
            </td>
            <td>
                <label class="label" id="lblDifficulty"></label>
            </td>
        </tr>
        <tr>
            <td>
                <label class="label" for="lblIssueType">Type</label>
            </td>
            <td>
                <label class="label" id="lblIssueType"></label>
            </td>
        </tr>
        <tr>
            <td>
                <label class="label" for="lblIssuePriority">Priority</label>
            </td>
            <td>
                <label class="label" id="lblIssuePriority"></label>
            </td>
        </tr>
        <tr>
            <td>
                <label class="label" for="lblIssueStatus">Status</label>
            </td>
            <td>
                <label class="label" id="lblIssueStatus"></label>
            </td>
        </tr>
        <tr>
            <td>
                <label class="label" for="lblIssueProject">Project</label>
            </td>
            <td>
                <label class="label" id="lblIssueProject"></label>
            </td>
        </tr>
        <%--         <tr>
            <td>
                <label class="label" for="lblUsers">Assigned Users</label>
            </td>
            <td>
                <label class="label" id="lblUsers"></label>
            </td>
        </tr>--%>
    </table>
</body>
</html>
