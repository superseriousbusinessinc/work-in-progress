﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="projectView.aspx.cs" Inherits="WIP_Website.pages.projectView" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <script>
        $(document).ready(initProjectCreate);
        var projectInfo = null;

        function initProjectCreate() {
            $("#btnViewIssues").click(function () { loadProjectIssues(projectInfo); });
            $("#btnAddIssue").click(function () { loadCreateProjectIssueForm(projectInfo); });
        }

        function populateProjectView(project) {
            projectInfo = project;
            $("#projectViewTitle").html("Project: " + project.projectName);
            $("#lblProjectID").html(project.projectID);
            $("#lblProjectName").html(project.projectName);
            $("#lblDateStarted").html(project.dateStarted);
            $("#lblDueDate").html(project.dueDate);
            $("#lblProjectType").html(project.projectType.Name);
            $("#lblProjectPriority").html(project.projectPriority.Name);
            $("#lblProjectStatus").html(project.projectStatus.Name);
        }
    </script>
    <title></title>
</head>
<body>
    <div class="viewOptions">
        <button id="btnViewIssues" class="button">View Issues</button>
        <button id="btnAddIssue" class="button">Add Issues</button>
        <button id="btnEditProject" class="button">Edit</button>
        <button id="btnManageProjectUsers" class="button">Manage Users</button>
    </div>
    <table id="viewProject" class="form">
        <tr>
            <td class="formTitle" colspan="2" id="projectViewTitle">Project: 
            </td>
        </tr>
        <tr>
            <td>
                <label class="label" for="lblProjectID">Project ID</label>
            </td>
            <td>
                <label id="lblProjectID" class="label"></label>
            </td>
        </tr>
        <tr>
            <td>
                <label class="label" for="lblProjectName">Project Name</label>
            </td>
            <td>
                <label class="label" id="lblProjectName"></label>
            </td>
        </tr>
        <tr>
            <td>
                <label class="label" for="lblDateStarted">Date Started</label>
            </td>
            <td>
                <label class="label" id="lblDateStarted"></label>
            </td>
        </tr>
        <tr>
            <td>
                <label class="label" for="lblDueDate">Due Date</label>
            </td>
            <td>
                <label class="label" id="lblDueDate"></label>
            </td>
        </tr>
        <tr>
            <td>
                <label class="label" for="lblProjectType">Type</label>
            </td>
            <td>
                <label class="label" id="lblProjectType"></label>
            </td>
        </tr>
        <tr>
            <td>
                <label class="label" for="lblProjectPriority">Priority</label>
            </td>
            <td>
                <label class="label" id="lblProjectPriority"></label>
            </td>
        </tr>
        <tr>
            <td>
                <label class="label" for="lblProjectStatus">Status</label>
            </td>
            <td>
                <label class="label" id="lblProjectStatus"></label>
            </td>
        </tr>
        <%--        <tr>
            <td>
                <label class="label" for="lblProjectComments">Comments</label>
            </td>
            <td>
                <label class="label" id="lblProjectComments"></label>
            </td>
        </tr>
        <tr>
            <td>
                <label class="label" for="lblProjectUsers">Assigned User</label>
            </td>
            <td>
                <label class="label" id="lblProjectUsers"></label>
            </td>
        </tr>--%>
    </table>
</body>
</html>
