﻿$(document).ready(init);

var session = {
    user_ID: null,
    username: null,
    company_ID: null,
    companyName: null
};

//Modal
var modal = null;
var mTop = null;
var mTitle = null;
var mBody = null;
var mBottom = null;
var shadow = null;

function init() {
    checkSessionUser();

    modal = document.getElementById('formModal');
    mTop = document.getElementById('mTop');
    mTitle = document.getElementById('mTitle');
    mBody = document.getElementById('mBody');
    mBottom = document.getElementById('mBottom');
    shadow = document.getElementById('mShadow');

    window.onresize = resizeWin;
    resizeWin();
}

function checkSessionUser() {
    $.ajax({
        url: "login.aspx/GetSession",
        type: "POST",
        contentType: "application/json",
        dataType: "JSON",
        timeout: 60000,
        success: function (result) {
            console.log("checkSessionUser: SUCCESS");
            console.log(result);
            if (result.d == null) {
                window.location.href = "login.aspx";
            } else {
                session.user_ID = result.d.user_ID;
                session.username = result.d.username;
                session.company_ID = result.d.company_ID;
                session.companyName = result.d.companyName;
                $("#signedInAs").html("Signed in as: " + session.username);
            }
        },
        error: function (xhr, status) {
            console.log("checkSessionUser: ERROR");
            showError(xhr, status);
        }
    });
}

function logout() {
    $.ajax({
        url: "dashboard.aspx/Logout",
        type: "POST",
        contentType: "application/json",
        dataType: "JSON",
        timeout: 60000,
        success: function (result) {
            console.log("logout: SUCCESS");
            console.log(result);
            if (result.d) {
                window.location.href = "login.aspx";
            }
        },
        error: function (xhr, status) {
            console.log("logout: ERROR");
            console.log(xhr);
            console.log(status);
        }
    });
}


/*************** ADMIN TOOLS ***************/

function loadAdminTools() {
    hideAllModules();
    setNavSelected("navItemAdminTools", true);
    $("#mBody").html("");
    $("#adminToolsDiv").css("display", "block");
}

/*CREATE USER*/
function loadCreateUserForm() {
    showModal("forms/userCreate.aspx", initializeCreateUserForm, "Create User");
    //$("#mBody").load("forms/userCreate.aspx", initializeCreateUserForm);
}

function initializeCreateUserForm() {
    $("#createUserForm").submit(createUser);
}

/*************** Projects ***************/

function showProjectsDiv() {
    hideAllModules();
    setNavSelected("navItemMyProjects", true);
    $("#projectsDiv").css("display", "block");
}

function hideAllProjectDivs() {
    $("#projectsGrid").css("display", "none");
    $("#projectView").css("display", "none");
}

function showProjectsGrid() {
    hideAllProjectDivs();
    $("#projectsGrid").css("display", "block");
}

function showProjectView() {
    hideAllProjectDivs();
    $("#projectView").css("display", "block");
}

function setProjectsDivTitle(title) {
    $("#projectsTitle").html(title);
}

/*VIEW PROJECT*/
function loadProjectView(project) {
    $("#projectView").load("views/projectView.aspx", function () {
        populateProjectView(project);
        showProjectView();
    });
}

/*VIEW PROJECTS*/
function loadUserProjects() {
    setProjectsDivTitle("My Projects");
    showProjectsGrid();
    $.ajax({
        url: "dashboard.aspx/LoadUserProjects",
        type: "POST",
        contentType: "application/json",
        dataType: "JSON",
        timeout: 60000,
        success: function (result) {
            console.log("loadUserProjects: SUCCESS");
            //console.log(result.d);
            //$("#projectsDiv").html("SUCCESS");
            if (result.d) {
                showProjectsGrid();
                $("#projectsGrid").html("");
                createProjectGridItems(document.getElementById("projectsGrid"), result.d);
            }
        },
        error: function (xhr, status) {
            console.log("loadUserProjects: ERROR");
            showError("projectsDiv_hiddenMsg", xhr, status);
        }
    });
    //$("#projectsDiv").html("LOADING...");
    showProjectsDiv();
}

function createProjectGridItems(grid, pInfoArr) {
    for (var i = 0; i < pInfoArr.length; i++) {
        var newDiv = createProjectDiv(pInfoArr[i]);
        grid.appendChild(newDiv);
        //console.log(newDiv);
    }
}

function createProjectDiv(pInfo) {
    var newDiv = document.createElement("div");
    newDiv.className = "gridItem";
    var title = document.createElement("div");
    title.className = "title";
    title.innerHTML = pInfo.projectName;
    title.onclick = function () { loadProjectView(pInfo) };
    var table = document.createElement("table");
    table.className = "itemInfo";
    //ProjectID
    var row = document.createElement("tr");
    var td1 = document.createElement("td");
    var td2 = document.createElement("td");
    td1.className = "infoHeader";
    td1.innerHTML = "ProjectID: ";
    td2.innerHTML = pInfo.projectID;
    row.appendChild(td1);
    row.appendChild(td2);
    table.appendChild(row);
    //DateStarted
    row = document.createElement("tr");
    td1 = document.createElement("td");
    td2 = document.createElement("td");
    td1.className = "infoHeader";
    td1.innerHTML = "Date Started: ";
    td2.innerHTML = pInfo.dateStarted;
    row.appendChild(td1);
    row.appendChild(td2);
    table.appendChild(row);
    //DateStarted
    row = document.createElement("tr");
    td1 = document.createElement("td");
    td2 = document.createElement("td");
    td1.className = "infoHeader";
    td1.innerHTML = "Date Due: ";
    td2.innerHTML = pInfo.dueDate;
    row.appendChild(td1);
    row.appendChild(td2);
    table.appendChild(row);
    //Type
    row = document.createElement("tr");
    td1 = document.createElement("td");
    td2 = document.createElement("td");
    td1.className = "infoHeader";
    td1.innerHTML = "Project Type: ";
    td2.innerHTML = pInfo.projectType.Name;
    row.appendChild(td1);
    row.appendChild(td2);
    table.appendChild(row);
    //Priority
    row = document.createElement("tr");
    td1 = document.createElement("td");
    td2 = document.createElement("td");
    td1.className = "infoHeader";
    td1.innerHTML = "Priority: ";
    td2.innerHTML = pInfo.projectPriority.Name;
    row.appendChild(td1);
    row.appendChild(td2);
    table.appendChild(row);
    //Status
    row = document.createElement("tr");
    td1 = document.createElement("td");
    td2 = document.createElement("td");
    td1.className = "infoHeader";
    td1.innerHTML = "Status: ";
    td2.innerHTML = pInfo.projectStatus.Name;
    row.appendChild(td1);
    row.appendChild(td2);
    table.appendChild(row);
    //Status
    row = document.createElement("tr");
    td1 = document.createElement("td");
    td2 = document.createElement("td");
    var btnViewIssues = document.createElement("button");
    btnViewIssues.className = "button";
    btnViewIssues.innerHTML = "View Issues";
    btnViewIssues.onclick = function () { loadProjectIssues(pInfo); };
    td1.colSpan = 2;
    td1.style.textAlign = "center";
    td1.appendChild(btnViewIssues);
    //var btnCreateIssue = document.createElement("button");
    //btnCreateIssue.className = "button";
    //btnCreateIssue.innerHTML = "Create Issue";
    //btnCreateIssue.onclick = function () { loadCreateProjectIssueForm(pInfo); };
    //td2.appendChild(btnCreateIssue);
    row.appendChild(td1);
    row.appendChild(td2);
    table.appendChild(row);

    newDiv.appendChild(title);
    newDiv.appendChild(table);
    return newDiv;
}

/*CREATE PROJECT*/
function loadCreateProjectForm() {
    showModal("forms/projectCreate.aspx", initializeCreateProjectForm, "Create Project");
}

function initializeCreateProjectForm() {
    $("#createProjectForm").submit(createProject);
}


/*************** ISSUES ***************/

function showIssuesDiv() {
    hideAllModules();
    setNavSelected("navItemMyIssues", true);
    $("#issuesDiv").css("display", "block");
}

function hideAllIssuesDivs() {
    $("#issuesGrid").css("display", "none");
    $("#issueView").css("display", "none");
}

function showIssuesGrid() {
    hideAllIssuesDivs();
    $("#issuesGrid").css("display", "block");
}

function showIssueView() {
    hideAllIssuesDivs();
    $("#issueView").css("display", "block");
}

function setIssuesDivTitle(title) {
    $("#issuesTitle").html(title);
}

/*VIEW ISSUE*/
function loadIssueView(issue) {
    $("#issueView").load("views/issueView.aspx", function () {
        populateIssueView(issue);
        showIssueView();
    });
}

/*VIEW ISSUES*/
function loadUserIssues() {
    setIssuesDivTitle("My Issues");
    showIssuesGrid();
    $.ajax({
        url: "dashboard.aspx/LoadUserIssues",
        type: "POST",
        contentType: "application/json",
        dataType: "JSON",
        timeout: 60000,
        success: function (result) {
            console.log("loadUserIssues: SUCCESS");
            //console.log(result.d);
            //$("#projectsDiv").html("SUCCESS");
            if (result.d) {
                showIssuesGrid();
                $("#issuesGrid").html("");
                createIssueGridItems(document.getElementById("issuesGrid"), result.d);
            }
        },
        error: function (xhr, status) {
            console.log("loadUserIssues: ERROR");
            showError("issuesDiv_hiddenMsg", xhr, status);
        }
    });
    //$("#projectsDiv").html("LOADING...");
    showIssuesDiv();
}

function loadProjectIssues(pInfo) {
    setIssuesDivTitle('Project "' + pInfo.projectName + '" Issues');
    showIssuesGrid();
    $.ajax({
        url: "dashboard.aspx/LoadProjectIssues",
        data: JSON.stringify({
            projectID: pInfo.projectID
        }),
        type: "POST",
        contentType: "application/json",
        dataType: "JSON",
        timeout: 60000,
        success: function (result) {
            console.log("loadProjectIssues: SUCCESS");
            if (result.d) {
                showIssuesGrid();
                $("#issuesGrid").html("");
                createIssueGridItems(document.getElementById("issuesGrid"), result.d);
            }
        },
        error: function (xhr, status) {
            console.log("loadProjectIssues: ERROR");
            showError("issuesDiv_hiddenMsg", xhr, status);
        }
    });
    hideAllModules();
    $("#issuesDiv").css("display", "block");
}

function createIssueGridItems(grid, iInfoArr) {
    for (var i = 0; i < iInfoArr.length; i++) {
        var newDiv = createIssueDiv(iInfoArr[i]);
        grid.appendChild(newDiv);
        //console.log(newDiv);
    }
}

function createIssueDiv(iInfo) {
    var newDiv = document.createElement("div");
    newDiv.className = "gridItem";
    var title = document.createElement("div");
    title.className = "title";
    title.innerHTML = iInfo.issueName;
    title.onclick = function () { loadIssueView(iInfo) };
    var table = document.createElement("table");
    table.className = "itemInfo";
    //IssueID
    var row = document.createElement("tr");
    var td1 = document.createElement("td");
    var td2 = document.createElement("td");
    td1.className = "infoHeader";
    td1.innerHTML = "IssueID: ";
    td2.innerHTML = iInfo.issueID;
    row.appendChild(td1);
    row.appendChild(td2);
    table.appendChild(row);
    //ProjectName
    row = document.createElement("tr");
    td1 = document.createElement("td");
    td2 = document.createElement("td");
    td1.className = "infoHeader";
    td1.innerHTML = "Project: ";
    td2.innerHTML = iInfo.project.ProjectName;
    row.appendChild(td1);
    row.appendChild(td2);
    table.appendChild(row);
    //DateCreated
    row = document.createElement("tr");
    td1 = document.createElement("td");
    td2 = document.createElement("td");
    td1.className = "infoHeader";
    td1.innerHTML = "Date Created: ";
    td2.innerHTML = iInfo.dateCreated;
    row.appendChild(td1);
    row.appendChild(td2);
    table.appendChild(row);
    //EstimatedTime
    row = document.createElement("tr");
    td1 = document.createElement("td");
    td2 = document.createElement("td");
    td1.className = "infoHeader";
    td1.innerHTML = "Estimated Time: ";
    td2.innerHTML = iInfo.estimatedTime;
    row.appendChild(td1);
    row.appendChild(td2);
    table.appendChild(row);
    //Difficulty
    row = document.createElement("tr");
    td1 = document.createElement("td");
    td2 = document.createElement("td");
    td1.className = "infoHeader";
    td1.innerHTML = "Difficulty: ";
    td2.innerHTML = iInfo.difficulty;
    row.appendChild(td1);
    row.appendChild(td2);
    table.appendChild(row);
    //Type
    row = document.createElement("tr");
    td1 = document.createElement("td");
    td2 = document.createElement("td");
    td1.className = "infoHeader";
    td1.innerHTML = "Type: ";
    td2.innerHTML = iInfo.issueType.Name;
    row.appendChild(td1);
    row.appendChild(td2);
    table.appendChild(row);
    //Priority
    row = document.createElement("tr");
    td1 = document.createElement("td");
    td2 = document.createElement("td");
    td1.className = "infoHeader";
    td1.innerHTML = "Priority: ";
    td2.innerHTML = iInfo.issuePriority.Name;
    row.appendChild(td1);
    row.appendChild(td2);
    table.appendChild(row);
    //Status
    row = document.createElement("tr");
    td1 = document.createElement("td");
    td2 = document.createElement("td");
    td1.className = "infoHeader";
    td1.innerHTML = "Status: ";
    td2.innerHTML = iInfo.issueStatus.Name;
    row.appendChild(td1);
    row.appendChild(td2);
    table.appendChild(row);

    newDiv.appendChild(title);
    newDiv.appendChild(table);
    return newDiv;
}

/*CREATE ISSUE*/
function loadCreateProjectIssueForm(pInfo) {
    showModal("forms/issueCreate.aspx", function () {
        initializeCreateIssueForm(pInfo);
    }, "Create Issue");
}

function initializeCreateIssueForm(pInfo) {
    $("#createIssueForm").submit(function () { createIssue(pInfo.projectID); });
    loadProjectForIssueForm(pInfo);
}


/*************** Utility Functions ***************/

function hideAllModules() {
    $("#adminToolsDiv").css("display", "none");
    $("#btnAdminTools_ClearForm").css("display", "none");
    $("#projectsDiv").css("display", "none");
    $("#issuesDiv").css("display", "none");

    //set all navitems as not selected.
    $("#navItemMyProjects").removeClass("selected");
    $("#navItemMyIssues").removeClass("selected");
    $("#navItemAdminTools").removeClass("selected");
}

function setNavSelected(id, isSelected) {
    if (isSelected)
        $("#" + id).addClass("selected");
    else
        $("#" + id).removeClass("selected");
}

function showError(divId, xhr, status) {
    $("#" + divId).css("display", "inline");
    $("#" + divId).css("color", "red");
    $("#" + divId).html("Server Error. Check developer console for more details.");
    console.log("xhr: ");
    console.log(xhr);
    console.log(status);
}


/*************** Modal Functions ***************/

function showModal(resourceName, callback, title) {
    if (title)
        mTitle.innerHTML = title;
    $("#mBody").load(resourceName, function () {
        callback();
        fadeElement(shadow, true, 25);
        fadeElement(modal, true, 25);
        resizeWin();
    });
    $("#mHiddenMsg").css("display", "none");
}

function hideModal() {
    fadeElement(shadow, false, 10);
    fadeElement(modal, false, 10);
}

function fadeElement(el, blnFadeIn, timeout) {
    var opacity = Number(el.style.opacity);
    if (blnFadeIn) {
        el.style.display = 'inline';
        if (opacity < 1.0) {
            el.style.opacity = opacity + 0.1;
            setTimeout(function () { fadeElement(el, true, timeout); }, timeout);
        }
    } else {
        if (opacity > 0.0) {
            el.style.opacity -= 0.1;
            setTimeout(function () { fadeElement(el, false, timeout); }, timeout);
        } else {
            el.style.display = 'none';
        }
    }
}