﻿$(document).ready(init);

function init() {
    window.onresize = resizeWin;
    resizeWin();
}

//Resize the mainContentDiv to fit the window.
function resizeWin() {
    var w = window.innerWidth;
    var h = window.innerHeight;

    var head = document.getElementById("head");
    var nav = document.getElementById("nav");
    var mainDiv = document.getElementById("main");

    head.style.width = (w - 35) + "px";

    nav.style.width = (head.offsetWidth / 5) + "px";
    nav.style.height = (h - head.offsetHeight - 25) + "px";

    mainDiv.style.marginLeft = (nav.offsetWidth) + "px";
    mainDiv.style.width = (head.offsetWidth - nav.offsetWidth - 14) + "px";
    mainDiv.style.height = (h - head.offsetHeight - 37) + "px";

    var modal = document.getElementById('formModal');
    var mTop = document.getElementById('mTop');
    var mBody = document.getElementById('mBody');
    var mBottom = document.getElementById('mBottom');
    if (modal && mTop && mBody && mBottom) {
        //Set modal dimensions and location.
        modal.style.width = (w / 2) + 'px'
        //modal.style.height = (wHeight / 2) + 'px';
        modal.style.maxHeight = (h / 2) + 'px';
        //mBody.style.height = (modal.offsetHeight - (mTop.offsetHeight + mBottom.offsetHeight) - 20) + 'px';
        mBody.style.maxHeight = ((h / 2) - (mTop.offsetHeight + mBottom.offsetHeight) - 20) + 'px';
        var mTopPos = (h / 2) - (modal.offsetHeight / 2);
        var mLeftPos = (w / 2) - (modal.offsetWidth / 2);
        modal.style.top = mTopPos + 'px';
        modal.style.left = mLeftPos + 'px';
    }
}