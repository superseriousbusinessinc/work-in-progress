﻿$(document).ready(init);

function init() {
    checkSessionUser();

    $("#loginForm").submit(function (e) {
        e.preventDefault();
    });
}

function checkSessionUser() {
    $.ajax({
        url: "login.aspx/GetSession",
        type: "POST",
        contentType: "application/json",
        dataType: "JSON",
        timeout: 60000,
        success: function (result) {
            console.log("checkSessionUser: SUCCESS");
            console.log(result);
            if (result.d != null) {
                window.location.href = "dashboard.aspx";
            }
        },
        error: function (xhr, status) {
            console.log("checkSessionUser: ERROR");
            showError(xhr, status);
        }
    });
}

function login() {
    $.ajax({
        url: "login.aspx/Login",
        data: JSON.stringify({
            username: $("#txtUsername").val(),
            password: $("#txtPassword").val()
        }),
        type: "POST",
        contentType: "application/json",
        dataType: "JSON",
        timeout: 60000,
        success: function (result) {
            var response = result.d;
            if (response != null)
                loginSuccess(response);
            else
                loginFailure(response);
        },
        error: function (xhr, status) {
            console.log("login: ERROR");
            showError(xhr, status);
        }
    });
}

function loginSuccess(user) {
    $("#hiddenMsg").css("display", "inline");
    $("#hiddenMsg").css("color", "green");
    $("#hiddenMsg").html("Login Success!  User_ID: " + user.user_ID + ", Username: " + user.username);
    window.location.href = "dashboard.aspx";
}

function loginFailure(user) {
    $("#hiddenMsg").css("display", "inline");
    $("#hiddenMsg").css("color", "red");
    $("#hiddenMsg").html("Login Failed.  Invalid Username or Password.");
}

function showError(xhr, status) {
    $("#hiddenMsg").css("display", "inline");
    $("#hiddenMsg").css("color", "red");
    $("#hiddenMsg").html("Server Error. Check developer console for more details.");
    console.log("xhr: ");
    console.log(xhr);
    console.log(status);
}